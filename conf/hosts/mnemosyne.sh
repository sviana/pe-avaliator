#!/bin/bash

echo "Configuration for machine mnemosyne"
# set the environment
VIRTUALENV_NAME=mnemy-env
PE_AVALIATOR_PATH=$HOME/work/pe-avaliator
VIRTUALENV=$HOME/work/$VIRTUALENV_NAME
USE_CORES=7
BLASTDB=/home/sviana/work/SILVA/SILVA_119_SSURef_Nr99_tax_silva.DNA
