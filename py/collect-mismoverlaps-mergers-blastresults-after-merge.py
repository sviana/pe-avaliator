import time

from commons import *

__author__ = 'sam'


def allocate_matrix(row_ids, col_ids):
    new_matrix = dict()
    counter = 0
    for row_id in row_ids:
        if (counter % 10000 == 0):
            print "%d" % (counter) + "th (%f %%) row allocated ! " % (float(counter) / float(len(row_ids)) * 100.0)

        new_row = dict()
        new_row['id'] = row_id
        for col_id in col_ids:
            #            new_row[ col_id + "-identity" ] = "-"
            new_row[col_id + "-mismatches"] = "-"
        # new_row[ col_id + "-length"] = "-"
        #            new_row[ col_id + "-start"] = "-"
        #            new_row[ col_id + "-end"] = "-"
        new_matrix[row_id] = new_row
        counter += 1
    return new_matrix


def write_tabular(out_filename, data, col_ids):
    headers = ['id']
    for col_id in col_ids:
        headers += [col_id + "-identity", col_id + "-mismatches", col_id + "-length", col_id + "-start",
                    col_id + "-end"]

    # headers = ['id',
    #            'R1_001-subject', 'R1_001-identity', 'R1_001-mismatches',
    #            'R2_001-subject', 'R2_001-identity', 'R2_001-mismatches',
    #            'bbmerge-subject','bbmerge-identity','bbmerge-mismatches',
    #            'fastq-join-subject','fastq-join-identity', 'fastq-join-mismatches',
    #            'flash-subject','flash-identity','flash-mismatches',
    #            'mothur-subject','mothur-identity','mothur-mismatches',
    #            'pandaseq-subject','pandaseq-identity', 'pandaseq-mismatches',
    #            'pear-subject','pear-identity', 'pear-mismatches',
    #            'usearch-subject','usearch-identity', 'usearch-mismatches']

    # ['bbmerge-identity', 'fastq-join-identity', 'fastq-join-mismatches', 'pear-mismatches', 'R1_001-identity', 'flash-identity',
    #  'R2_001-identity', 'flash-mismatches', 'R2_001-mismatches', 'bbmerge-mismatches', 'pear-identity', 'R1_001-mismatches',
    # 'usearch-identity', 'usearch-mismatches', 'id']

    print headers

    writer = csv.DictWriter(open(out_filename, "w"), headers, delimiter="\t")
    writer.writeheader()
    # writer.writerow(data [ data.keys()[0]] )

    for (key, row) in data.iteritems():
        writer.writerow(row)


def main():
    cur_path = sys.argv[2]
    #    blastresults_forward = sys.argv[2]
    #    blastresults_reverse = sys.argv[3]
    fasta_filename = sys.argv[1]
    print "Fetching accessions from " + fasta_filename

    accessions = fetch_accessions(fasta_filename)

    blastresults_filenames = list_files(cur_path, "*.blastresults")

    filenames_tokens = sorted(extract_token_ids(blastresults_filenames))

    stats = allocate_matrix(accessions, filenames_tokens)

    print "Total stats slots: %d" % (len(stats))


    # os.chdir(cur_path)
    for f in blastresults_filenames:

        print "Parsing file " + f

        rows = tsv_fetch_rows(f, 0)

        lines_count = len(rows)

        counter = 0

        now = time.time()

        keys_not_found_counter = 0

        for seq_id in rows.keys():
            if (counter % 10000 == 0):
                last = now
                now = time.time()
                print "%d" % (counter) + "th (%f %%) row of blastresults file reached ! " % (
                    float(counter) / float(lines_count) * 100.0)

                # print "Time in between: %d seconds! " % ( now - last )

            csv_row = rows[seq_id]

            try:
                row = stats[seq_id]

                token_id = get_filename_token_id(f)


                #               row[ token_id +'-identity'] = csv_row[2]
                #                row[ token_id + '-length'] = csv_row[3]
                row[token_id + '-mismatches'] = csv_row[4]
            # row [ token_id + "-start"] = csv_row[8]
            #                row [ token_id + "-end"] = csv_row[9]
            except KeyError:
                keys_not_found_counter += 1

            counter += 1

    print "Keys not found: ", keys_not_found_counter

    write_to_report(["Total of read ids not found : %d" % (keys_not_found_counter)])

    print "The results were written to " + sys.argv[3]

    write_tabular(sys.argv[3], stats, filenames_tokens)


if __name__ == '__main__':
    main()
