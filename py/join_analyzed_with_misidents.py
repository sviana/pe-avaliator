import os

__author__ = 'sviana'

import sys

import commons


def concat_files(file1, file2, concated_file_name):
    commons.executing(' cat %s %s > %s' % (file1, file2, concated_file_name))


def main():
    dir = sys.argv[1]
    misident_blastresult_files = commons.list_files(dir, "*.subset.1by1.analyzed.blastresults")
    for file in misident_blastresult_files:
        main_file = file.replace(".subset.1by1", "")
        concated_filename = file.replace("subset.1by1", "joined")
        concat_files(file, main_file, concated_filename)
        os.rename(concated_filename, main_file)


# commons.executing('mv -v %s %s' % (concated_filename,main_file))


if __name__ == '__main__':
    main()
