import os
import sys

import commons


def get_blastresults_details():
    dir = sys.argv[1]

    if len(sys.argv) >= 3:
        blastresults_filename = sys.argv[2]
        fasta_filename = sys.argv[3]
    else:
        lmfile = commons.recent_mfile(dir).strip()
        if (not lmfile.endswith('.blastresults')):
            lastblastresults = filter(lambda x: x.endswith('.blastresults'), commons.recent_mfiles('.'))
            blastresults_filename = lastblastresults[0]
        else:
            blastresults_filename = lmfile

        fasta_filename = commons.basename(blastresults_filename) + ".fasta"

    os.chdir(dir)

    num_lines_fasta = commons.count_lines(fasta_filename)
    fasta_records_count = num_lines_fasta / 2


    curr_pos_blastresults = commons.exec_output("tail -n1 %s" % (blastresults_filename)).split("\t")[0]

    curr_pos_blastresults = ":".join(curr_pos_blastresults.split(":")[-3:-1])

    pos_record_fasta = commons.exec_output("grep -n \"%s\" %s" % (curr_pos_blastresults, fasta_filename)).split(":")[0]

    percent_complete = float(pos_record_fasta) / float(num_lines_fasta) * 100.0

    return (blastresults_filename, percent_complete)


if __name__ == '__main__':

    last = None

    filename, progress = get_blastresults_details()
    if last == progress:
        print "Terminated!"
    else:
        print progress, filename
