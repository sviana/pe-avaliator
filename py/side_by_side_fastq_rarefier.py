import sys
import random
import uuid

from Bio import SeqIO

import commons


def randstringofsize(char_range, times):
    buf = ''
    for i in range(1, times):
        buf += random.choice(char_range)
    return buf


def generate_random_strings():
    word_range = 'abcdefghijklmnopqrstuvwxyz1234567890*_,;'
    f = open('randstrings.txt', 'w')
    for x in range(0, 1000):
        rand_string = randstringofsize(word_range, 20)
        f.write(rand_string + "\n")
    f.close()


def rarify(rec1, rec2, target1, target2, rarify_factor, do_something):
    rand = random.random()
    if (rand < (1.0 / rarify_factor)):
        do_something(target1, rec1)
        do_something(target2, rec2)


def write_seq(target_handle, seq_rec):
    SeqIO.write(seq_rec, target_handle, 'fastq')


def main():
    # generate_random_strings()

    # sys.exit(-1)

    forw_fastq = sys.argv[1]
    rev_fastq = sys.argv[2]
    rarified_to_size_filename = sys.argv[3]

    format = commons.extension(forw_fastq)

    rarified_forw_fastq = commons.basename(forw_fastq) + '.rarified.' + format
    rarified_rev_fastq = commons.basename(rev_fastq) + '.rarified.' + format

    print "counting records on ", forw_fastq

    to_be_rarified_count = commons.fastx_count_records(forw_fastq)
    print "counting records on ", rarified_to_size_filename
    rarified_to_size_count = commons.fastx_count_records(rarified_to_size_filename)

    print "To be rarified records count: %d" % (to_be_rarified_count)

    print "Rarified to count records: %d" % (rarified_to_size_count)

    rarify_factor = float(to_be_rarified_count) / float(rarified_to_size_count)

    print "Rarify factor: %f" % (rarify_factor)

    random_char = random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')

    print "Fetching records from", forw_fastq
    seqs_forw = SeqIO.index(forw_fastq, 'fastq')  # , key_function=new_key)
    print "Fetcing records from", rev_fastq
    seqs_rev = SeqIO.index(rev_fastq, 'fastq')  #, key_function=get_last_key)

    target1 = open(rarified_forw_fastq, 'w')
    target2 = open(rarified_rev_fastq, 'w')

    for rec_id in seqs_forw:
        rec1, rec2 = seqs_forw[rec_id], seqs_rev[rec_id]
        rarify(rec1, rec2, target1, target2, rarify_factor, write_seq)


    print "Final sequences file is " + rarified_forw_fastq, rarified_rev_fastq

    print "Final sized of sequences file rarified: %d" % (commons.fastx_count_records(target1, format))

    target1.close()
    target2.close()


def get_random_char():
    random_char = random.choice('ABCDEF')
    last_char = random_char
    return random_char


last_char = None
last_new_keys = []
i = 0
j = 0


def new_key(orig_key):
    global i
    global last_new_keys
    #    var_portion = randstringofsize('1234567890',5) + ':' + randstringofsize('1234567890',4)
    var_portion = str(uuid.uuid1()).replace('-', '')
    last_new_key = orig_key + ':' + var_portion
    i += 1
    if i % 10000 == 0: print i
    last_new_keys.append(last_new_key)
    return last_new_key


def get_last_key(orig_key):
    global j
    global last_new_keys
    j += 1
    if j % 10000 == 0: print j
    return last_new_keys[j - 1]


if __name__ == '__main__':
    main()
