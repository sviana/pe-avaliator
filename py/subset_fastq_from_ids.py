# /usr/bin/env python

from Bio import SeqIO

__author__ = 'sviana'

import os
import sys
import commons


def main():
    fastx_dir = sys.argv[1]
    read_ids_dir = sys.argv[2]

    for fastx in commons.list_files(fastx_dir, '*MERGED.*.fastq'):
        read_ids_filename = read_ids_dir + os.sep + commons.strip_extension(
            os.path.basename(fastx)) + ".analyzed.misidentifieds.txt"
        isolate_and_blast_misidents(fastx, read_ids_filename)


def isolate_and_blast_misidents(original_fastx_name, read_ids_filename):
    read_ids = [line.strip() for line in open(read_ids_filename, 'r').readlines()]

    subset_fasta_name = commons.basename(original_fastx_name) + ".subset.fasta"

    print "Catching misidentifieds in  ", original_fastx_name

    all_reads_dict = SeqIO.index(original_fastx_name, commons.extension(original_fastx_name))

    found_seq_recs = list()

    print "Found ", len(all_reads_dict), " sequence records"

    i = 0

    for read_id in all_reads_dict.keys():

        seq_rec = all_reads_dict[read_id]
        i += 1

        if i % 10000 == 0:
            print  "sequence record ", i, " reached"

        if read_id in read_ids:
            found_seq_recs.append(seq_rec)

    SeqIO.write(found_seq_recs, subset_fasta_name, 'fasta')

    print "Wrote %d records to %s" % (len(found_seq_recs), subset_fasta_name)


if __name__ == '__main__':
    main()
