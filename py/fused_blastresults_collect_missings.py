import csv
import time

from Bio import SeqIO
from orderedset._orderedset import OrderedSet

import datetime
import commons

__author__ = 'sam'
import sys


def main():
    blastresults_filename = sys.argv[1]
    source_fasta_filename = sys.argv[2]

    missings_fasta_filename = blastresults_filename + ".missings.fasta"

    missings_ids_filename = blastresults_filename + ".missings.txt"


    # let's walk first through the blast results file and get all the accessions in it

    print sys.argv[0] + " : detecting the reads which were missed in the BLAST"

    print "Parsing file with BLAST results " + blastresults_filename

    csvfile = open(blastresults_filename, "r")

    sniffer = csv.Sniffer()

    sample = csvfile.readline()

    csvfile.seek(0)

    blastres_reader = csv.reader(open(blastresults_filename, "r"), delimiter="\t")

    if (sniffer.has_header(sample)):
        blastres_reader.next()  # skips the header


    i = 0
    missing = 0


    before = time.time()

    forward_read_ids = OrderedSet()
    reverse_read_ids = OrderedSet()

    blastres_numlines = commons.count_lines( blastresults_filename )

    print "Fetching read ids from blastresults file ..."

    for blastres_row in blastres_reader:
        read_id = blastres_row[0]
        direction = read_id.split(':')[-1]

        proper_read_id = read_id[0:read_id.rfind(':')]
        if direction == 'F':
            forward_read_ids.add(proper_read_id)
        elif direction == 'R':
            reverse_read_ids.add(proper_read_id)

        i += 1

        if i % 100000 == 0:
            print "Total blastresults lines verified: %d (%f%%)" % (i, float(i) / float(blastres_numlines) * 100.0)
            print "Time lasted since beginning: %d seconds" % int(time.time() - before)

    source_fasta = open(source_fasta_filename,"r")

    missings_fasta = open(missings_fasta_filename,"w")

    print "Total lines read:", i

    union_read_ids = forward_read_ids | reverse_read_ids

    print "Now reading FASTA with the original reads, and detecting which are the missings ...."

    orig_fasta_num_lines = commons.count_lines(source_fasta_filename) / 2

    before = time.time()

    i = 0

    missing_read_ids = set()

    for seq_record in SeqIO.parse( source_fasta, "fasta" ):
        read_id = seq_record.id
        proper_read_id = read_id[0:read_id.rfind(':')]

        if proper_read_id not in union_read_ids :
            missing_read_ids.add(proper_read_id)
            commons.write_seq_in_fasta_without_newlines( missings_fasta, seq_record)
            missing += 1
        if i % 100000 == 0:
            print "Total read ids tested: %d (%f%%)" % (i, float(i) / float(orig_fasta_num_lines) * 100.0)
            print "Time lasted since beginning: %d seconds" % int(time.time() - before)
        i += 1

    commons.write_list_to_textfile(missing_read_ids, missings_ids_filename)
    td = datetime.timedelta(time.time() - before)
#    print "Total time of execution: %s m, %s s " % (str(td.min), str(td.seconds))

    print "Missing total: ", missing / 2
    print "Missing read ids count:", len(missing_read_ids)


    print "The reads with no hits in blast results were written to " + missings_ids_filename

    print "And a  FASTA with those reads were written to " + missings_fasta_filename

    commons.write_to_report(
        ["Total of reads which couldn't blast hits: %d" % len(missing_read_ids)]
    )


if __name__ == '__main__':
    main()
