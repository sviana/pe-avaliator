__author__ = 'sviana'

import sys

import commons


def main():
    fastq_name_to_be_cleaned = sys.argv[1]
    print "Cleaning ", fastq_name_to_be_cleaned
    new_fastq_cleaned_name = commons.basename(fastq_name_to_be_cleaned) + '.header_cleaned.fastq'
    i = 0

    fastq = open(fastq_name_to_be_cleaned, "r")
    new_fastq = open(new_fastq_cleaned_name, "w")
    for line in fastq:
        line = line.rstrip()
        if i % 4 == 0:
            if line.find('/') > -1:
                line = line.split('/')[0]
        new_fastq.write(line + "\n")
        i += 1
    print "New FASTQ:", new_fastq_cleaned_name


if __name__ == '__main__':
    main()
