import sys

from Bio import SeqIO

import commons

__author__ = 'sam'


def common_reads():
    fastq_filename1 = sys.argv[1]
    fastq_filename2 = sys.argv[2]

    reads_final_filename = sys.argv[3]
    reads_final_file = open(reads_final_filename, "w")

    accessions1 = commons.fetch_accessions(fastq_filename1, "fastq")
    accessions2 = commons.fetch_accessions(fastq_filename2, "fastq")

    len_reads1 = len(accessions1)
    len_reads2 = len(accessions2)

    common_reads_keys = accessions1 & accessions2

    reads_final_file = open(reads_final_filename, "w")
    for seq_rec in SeqIO.parse(open(fastq_filename1), "fastq"):
        if seq_rec.name in common_reads_keys:
            SeqIO.write(seq_rec, reads_final_file, 'fastq')


if __name__ == '__main__':
    common_reads()
