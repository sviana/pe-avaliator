import sys

from Bio import SeqIO

import commons


def main():
    print "subset_fastq_from_fasta.py: Create a new FASTQ file using only the ids in another FASTA:"


    original_fastq_filename = sys.argv[1]
    fasta_with_ids_to_be_used = sys.argv[2]
    new_fastq_filename = sys.argv[3]

    print "Fetching accessions...."

    ids_to_be_used = commons.fetch_accessions(fasta_with_ids_to_be_used)

    fasta_count = len(ids_to_be_used)

    original_fastq_rec_count = commons.fastx_count_records(original_fastq_filename)

    print "Original FASTQ:", original_fastq_filename, original_fastq_rec_count, " records"
    print "FASTA with the ids:", fasta_with_ids_to_be_used, len(ids_to_be_used), " records (%f%%)" % (
    float(fasta_count) / original_fastq_rec_count * 100.0)
    print "NEW FASTQ:", new_fastq_filename

    new_fastq = open(new_fastq_filename, "w")

    for seq_rec in SeqIO.parse(original_fastq_filename, "fastq"):
        if seq_rec.name in ids_to_be_used:
            SeqIO.write(seq_rec, new_fastq, "fastq")

    print "Writing to %s completed!" % (new_fastq.name)
    #    print "Number lines: ", commons.count_lines( new_fastq_filename )

    new_fastq.close()

    new_fastq_records = commons.fastx_count_records(new_fastq_filename)
    print "Count records in %s : %d " % (new_fastq_filename, new_fastq_records)

    commons.write_to_report([
        "Number of reads in file %s : %d" % (new_fastq_filename, new_fastq_records)
    ])

if __name__ == '__main__':
    main()
