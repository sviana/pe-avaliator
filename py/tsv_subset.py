import csv

__author__ = 'sviana'

import os
import sys
import commons


def main():
    tsv_name = sys.argv[1]
    ids_filename = sys.argv[2]

    subset_filename = commons.basename(tsv_name) + "-" + commons.strip_extension(
        ids_filename.replace(os.path.commonprefix([tsv_name, ids_filename]), '')) + '.tsv'

    dict_rows = commons.tsv_fetch_rows(tsv_name)
    read_ids = [line.strip() for line in open(ids_filename, 'r').readlines()]

    subset_ids = set(dict_rows.keys()) & set(read_ids)

    subset_file_writer = csv.writer(open(subset_filename, 'w'), delimiter='\t')
    for id in subset_ids:
        subset_file_writer.writerow(dict_rows[id])

    print "Written ", len(subset_ids), " lines to " + subset_filename


if __name__ == '__main__':
    main()
