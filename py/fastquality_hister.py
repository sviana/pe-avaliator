import os
import sys
import collections

from Bio import SeqIO

import commons


def median(list):
    return list[len(list) / 2]


def avg(list):
    return float(sum(list)) / len(list)


def counts(list):
    return collections.Counter(list)


def plot_hist(list, fastq_name):
    import matplotlib.pyplot as pyplot
    pyplot.hist(list, log=True)
    pyplot.title("HISTOGRAM of " + os.path.basename(fastq_name) + " QUALITIES")
    pyplot.xlabel("Values")
    pyplot.ylabel("Frequencies")

    fig = pyplot.gcf()

    histogram_image_name = commons.basename(fastq_name) + '.qual.histogram.png'

    fig.savefig(histogram_image_name)

    os.system("xdg-open " + histogram_image_name)


def main():
    fastq_name = sys.argv[1]
    fastq_loc = os.path.dirname(fastq_name)
    #    scores = commons.walk_sequences(fastq_name,lambda seq_rec: (seq_rec.letter_annotations['phred_quality']) )
    scores = []
    for seq_rec in SeqIO.index(fastq_name, 'fastq').values():
        score = seq_rec.letter_annotations['phred_quality']
        scores.append(score)

    scores_flat = [item for sublist in scores for item in sublist]

    scores_sorted = sorted(scores_flat)
    # print scores_sorted


    print "------"

    print os.path.basename(fastq_name)

    print "median", median(scores_sorted)
    print "average", avg(scores_sorted)
    abs_frequencies = counts(scores_sorted)
    values_sorted = sorted(abs_frequencies.keys())

    tuple_freqs = [(val, abs_frequencies[val]) for val in values_sorted]
    # plot_hist(scores_sorted,fastq_name)
    print tuple_freqs
    #   for item in tuple_freqs:
    #       print item


if __name__ == '__main__':
    main()
