import sys

from Bio import SeqIO
import commons


def main():
    fasta_filename = sys.argv[1]

    cleaned_fasta_filename = commons.basename(fasta_filename) + ".cleaned." + commons.extension(fasta_filename)

    cleaned_fasta = open(cleaned_fasta_filename, "w")

    for rec in SeqIO.parse(fasta_filename, commons.extension(fasta_filename)):
        new_id = rec.id.replace('_', ':')
        rec.id = new_id
        rec.description = new_id
        commons.write_seq_in_fasta_without_newlines(cleaned_fasta, rec)


# SeqIO.write(rec,cleaned_fasta,commons.extension(fasta_filename))



if __name__ == '__main__':
    main()
