import csv
import time

from orderedset import OrderedSet

import commons

__author__ = 'sam'
import sys


def detect_maximizer_identity(hsps_some_read):
    organism_max_key = None
    organism_max_val = 0.0
    for organism, data in hsps_some_read.iteritems():
        sum = 0.0
        if data.has_key('F') and data['F']['score'] != None:
            sum += data['F']['score']
        if data.has_key('R') and data['R']['score'] != None:
            sum += data['R']['score']
        if sum > organism_max_val:
            organism_max_val = sum
            organism_max_key = organism
    return organism_max_key


def main():
    blastresults_filename = sys.argv[1]

    compared_blastresults_filename = commons.basename(blastresults_filename) + ".analyzed.blastresults"

    # let's walk first through the blast results file and get all the accessions in it


    print "Parsing file with BLAST results " + blastresults_filename

    csvfile = open(blastresults_filename, "r")

    sniffer = csv.Sniffer()

    sample = csvfile.readline()

    csvfile.seek(0)

    blastres_reader = csv.reader(open(blastresults_filename, "r"), delimiter="\t")

    if (sniffer.has_header(sample)):
        blastres_reader.next()  # skips the header

    i = 0
    missing = 0

    before = time.time()

    collected_blastres_read_ids = OrderedSet()

    print "Counting the number of lines in " + blastresults_filename + ", for statistical purposes, just wait . . . "

    blastres_numlines = commons.count_lines(blastresults_filename)

    print "Total lines in ", blastresults_filename, ":", blastres_numlines

    read_id = None

    compared_blastresults_writer = csv.writer(open(compared_blastresults_filename, "w"), delimiter="\t")

    hsps_some_read = dict()

    first_line_for_read = True

    for blastres_row in blastres_reader:

        last_read_id = read_id

        read_id_bare = blastres_row[0]
        direction = read_id_bare.split(':')[-1]
        read_id = read_id_bare[0:read_id_bare.rfind(':')]

        if (first_line_for_read == False and last_read_id != read_id):
            best_organism = detect_maximizer_identity(hsps_some_read)
            entry = hsps_some_read[best_organism]
            if entry.has_key('F'):
                compared_blastresults_writer.writerow(entry['F']['row_data'])
            if entry.has_key('R'):
                compared_blastresults_writer.writerow(entry['R']['row_data'])

            first_line_for_read = True

            hsps_some_read = dict()

        organism = blastres_row[1]
        seq_identity = float(blastres_row[2])
        alignm_length = int(blastres_row[3])
        new_item = {'organism': organism, 'row_data': blastres_row, 'score': seq_identity * alignm_length}
        if organism in hsps_some_read:
            hsps_some_read[organism][direction] = new_item

        else:
            hsps_some_read[organism] = dict()
            hsps_some_read[organism][direction] = new_item

        i += 1
        if i % 100000 == 0:
            print "Total blastresults lines verified: %d (%f%%)" % (i, float(i) / float(blastres_numlines) * 100.0)

            print "Time lasted since beginning: %d seconds" % int(time.time() - before)
            #       print "Size of set accessions: %f MB" % commons.count_size_megs(all_accessions)

        first_line_for_read = False

    print "Total lines read:", i







    # td = datetime.timedelta(time.time() - before)
    # print "Total time of execution: %d m, %s " % td.min, td.seconds


if __name__ == '__main__':
    main()
