import csv
import sys

import commons


def fetch_tsv_blastresults_before_data(blastresults_before_merge_filename):
    reader = csv.reader(open(blastresults_before_merge_filename, "r"), delimiter="\t")

    blastresults_before_merge_data = dict()

    for row in reader:
        read_id_with_direction = row[0]
        proper_read_id = commons.get_proper_read_id(read_id_with_direction)
        direction = row[0].split(':')[-1]

        try:
            bucket = blastresults_before_merge_data[proper_read_id]

        except KeyError:
            bucket = {'sum': 0}

        bucket[direction] = row[4]
        bucket['sum'] += int(row[4])

        blastresults_before_merge_data[proper_read_id] = bucket

    return blastresults_before_merge_data


def main():
    blastresults_before_filename = sys.argv[1]
    blastresults_after_filename = sys.argv[2]

    blastresults_diff_filename = commons.basename(blastresults_after_filename) + ".diff.tsv"

    blastresults_before_data = fetch_tsv_blastresults_before_data(blastresults_before_filename)

    print len(blastresults_before_data)

    #    for i in range(0,10):
    #        print blastresults_before_data.popitem()


    reader = csv.DictReader(open(blastresults_after_filename, "r"), delimiter="\t")

    mergers_names = map(lambda name: name.replace('-mismatches', ''),
                        filter(lambda name: name.find('mismatches') >= 0, reader.fieldnames))

    print mergers_names

    writer = csv.DictWriter(open(blastresults_diff_filename, "w"), ['id'] + mergers_names, delimiter="\t")

    writer.writeheader()

    missings = 0

    for after_merge_row in reader:
        was_error = False
        read_id = after_merge_row['id']
        try:
            read_before_merge_data = blastresults_before_data[read_id]
        except KeyError:
            missings += 1
            was_error = True

        if not was_error:
            diff_data = {'id': read_id}

            for merger in mergers_names:

                mismatches_str = after_merge_row[merger + '-mismatches']

                if mismatches_str.strip() == '-':
                    diff_data[merger] = '-'
                else:
                    mismatches_value = int(mismatches_str)
                    diff_data[merger] = mismatches_value - int(read_before_merge_data['sum'])

            writer.writerow(diff_data)

    print "Total missings: ", missings

    commons.write_to_report(
        ["On calculating differences on the total of mismatches there were not found %d read ids" % missings])

    print "The differences in mismatches before and after merge were written to ", blastresults_diff_filename

    # hp = hpy()
    # print "Heap:\n", hp.heap()


if __name__ == '__main__':
    main()
