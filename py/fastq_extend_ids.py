import sys
import random
import uuid

from Bio import SeqIO

import commons


def randstringofsize(char_range, times):
    buf = ''
    for i in range(1, times + 1):
        buf += random.choice(char_range)
    return buf


def generate_random_strings():
    word_range = 'abcdefghijklmnopqrstuvwxyz1234567890*_,;'
    f = open('randstrings.txt', 'w')
    for x in range(0, 1000):
        rand_string = randstringofsize(word_range, 20)
        f.write(rand_string + "\n")
    f.close()


def write_seq(target_handle, seq_rec):
    SeqIO.write(seq_rec, target_handle, 'fastq')


def get_last_random_char():
    global last_random_char
    return last_random_char


def get_last_new_key():
    global last_new_key
    return last_new_key


def new_random_char():
    global last_random_char
    last_random_char = randstringofsize('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 10)
    #   print last_random_char
    return last_random_char


last_new_key = None


def main():
    global i
    global j

    forw_fastq_filename = sys.argv[1]
    rev_fastq_filename = sys.argv[2]

    forw_fastq = open(forw_fastq_filename, "r")
    rev_fastq = open(rev_fastq_filename, "r")

    forw_fastq_new_ids = open(commons.basename(forw_fastq_filename) + ".new_ids.fastq", "w")
    rev_fastq_new_ids = open(commons.basename(rev_fastq_filename) + ".new_ids.fastq", "w")

    line_counter = 0

    for line_forw in forw_fastq:
        line_rev = rev_fastq.next()

        if line_counter % 4 == 0:
            forw_header_tokens = line_forw.split(' ')
            rev_header_tokens = line_rev.split(' ')
            new_generated_key = new_key(forw_header_tokens[0])
            new_forw_header = ' '.join([new_generated_key, forw_header_tokens[1]])
            new_rev_header = ' '.join([new_generated_key, rev_header_tokens[1]])
            forw_fastq_new_ids.write(new_forw_header)
            rev_fastq_new_ids.write(new_rev_header)
        else:
            forw_fastq_new_ids.write(line_forw)
            rev_fastq_new_ids.write(line_rev)

        if line_counter % 10000 == 0: print line_counter

        line_counter += 1


def new_key(orig_key):
    global k
    global last_new_keys
    var_portion = str(uuid.uuid1()).replace('-', '')[0:10]
    #    tokens = orig_key.split(':')
    #    var_portion = randstringofsize('1234567890',5) + ':' +  randstringofsize('1234567890',4)
    #    last_new_key = ":".join(tokens[:-2]) + ':' + var_portion
    last_new_key = orig_key + ':' + var_portion
    # print last_new_key
    #    k += 1
    #    if k % 10000 == 0: print k
    # last_new_keys.append(last_new_key)
    return last_new_key


def new_uuid(orig_key):
    global i
    i += 1
    last_new_key = str(uuid.uuid1())
    if i % 10000 == 0: print i
    return last_new_key


if __name__ == '__main__':
    main()
