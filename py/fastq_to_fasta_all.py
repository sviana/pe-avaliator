import glob

import os,sys
import commons



def main():
    fastq_source_dir_name = sys.argv[1]

    for fastq_filename in commons.list_files(fastq_source_dir_name, "*.fastq"):
        fasta = commons.fastq_to_fasta_p(fastq_filename)

if __name__ == '__main__':
    main()
