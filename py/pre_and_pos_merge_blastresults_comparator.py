from collections import OrderedDict
import csv
import sys
import time

from orderedset._orderedset import OrderedSet

import commons

__author__ = 'sam'


def main():
    merged_blastresults_filename = sys.argv[1]

    before_merge_analyzed_blastresults_filename = sys.argv[2]

    merged_blastresults_analyzed_filename = commons.basename(merged_blastresults_filename) + ".analyzed.blastresults"

    # let's walk first through all the 'before merge' blastresults file and get all the accessions in it

    blastres_numlines = commons.count_lines(merged_blastresults_filename)

    print "Parsing file with merged BLAST results " + merged_blastresults_filename

    csvfile = open(merged_blastresults_filename, "r")

    sniffer = csv.Sniffer()

    sample = csvfile.readline()

    csvfile.seek(0)

    blastres_reader = csv.reader(open(merged_blastresults_filename, "r"), delimiter="\t")

    if (sniffer.has_header(sample)):
        blastres_reader.next()  # skips the header

    i = 0

    before = time.time()

    before_merge_blastres_reader = csv.reader(open(before_merge_analyzed_blastresults_filename, "r"), delimiter="\t")

    collected_blastres_data = OrderedDict()

    for csv_row in before_merge_blastres_reader:
        # accession = csv_row[0]
        accession = ":".join(csv_row[0].split(":")[:-1])
        organism = csv_row[1]
        if (organism.find("|") > 0):
            organism = organism.split("|")[1]
        collected_blastres_data[accession] = organism

    print "Total pre-merge blastresults rows: ", len(collected_blastres_data)


    #    one_entry = collected_blastres_data.keys().pop()

    #    print one_entry

    #    exit(-1)


    read_id = None


    #

    compared_blastresults_writer = csv.writer(open(merged_blastresults_analyzed_filename, "w"), delimiter="\t")

    hsps_some_read = OrderedDict()

    first_line_for_read = True

    blastres_num_lines = 0

    missing_read_ids = []

    reads_misidentified = OrderedSet()


    for blastres_row in blastres_reader:

        last_read_id = read_id

        read_id = blastres_row[0]


        if (first_line_for_read == False and last_read_id != read_id):

            try:
                organism = collected_blastres_data[last_read_id]
            except KeyError:
                missing_read_ids.append(last_read_id)

            try:
                entry = hsps_some_read[organism]
                compared_blastresults_writer.writerow(entry['row_data'])

            except:
                #                entry = hsps_some_read.values()[0]
                reads_misidentified.add(last_read_id)
                # print "Misidentified %s on %s " % (organism, last_read_id)


            first_line_for_read = True

            hsps_some_read = dict()

        organism = blastres_row[1]
        seq_identity = float(blastres_row[2])
        alignm_length = int(blastres_row[3])
        mismatches = int(blastres_row[4])

        new_item = {'organism': organism, 'row_data': blastres_row}

        if organism in hsps_some_read:
            hsps_some_read[organism] = new_item
        else:
            hsps_some_read[organism] = dict()
            hsps_some_read[organism] = new_item

        i += 1
        if i % 100000 == 0:
            print "Total blastresults lines verified: %d (%f%%)" % (i, float(i) / float(blastres_numlines) * 100.0)

            print "Time lasted since beginning: %d seconds" % int(time.time() - before)
            #       print "Size of set accessions: %f MB" % commons.count_size_megs(all_accessions)

        first_line_for_read = False

    print "Total lines read:", i

    report_lines = ["Total missing reads: %d" % len(missing_read_ids),
                    "Total misidentified reads: %d" % len(reads_misidentified)]

    print report_lines

    commons.write_to_report(report_lines)

    # commons.executing("mv -v %s analyzed/%s" % (merged_blastresults_analyzed_filename,merged_blastresults_analyzed_filename))

    missings_file = open(commons.basename(merged_blastresults_analyzed_filename) + ".missings.txt", "w")

    missings_file.write("\n".join(missing_read_ids))

    print "A file with the ids missed on blast hits are stored in " + missings_file.name


    misidentifieds_file = open(commons.basename(merged_blastresults_analyzed_filename) + ".misidentifieds.txt", "w")

    misidentifieds_file.write("\n".join(reads_misidentified))

    print "A file with the misidentifieds where stored in " + missings_file.name



if __name__ == '__main__':
    main()
