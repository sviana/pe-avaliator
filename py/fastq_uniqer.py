from commons import *

__author__ = 'sam'


# read one FASTQ entirely into memory in the form a dictionary, using the name (identifier) as the key, and the sequence
# data as the value
# uses BioPython to parse the SeqRecords
# in the end writes the unique reads into two fasta files

def save_dict_fasta(filename, fasta_dict):
    f = open(filename, "w")
    for (key, value) in fasta_dict.iteritems():
        f.write(">" + key + "\n" + value + "\n")


def save_list(filename, list):
    f = open(filename, "w")
    f.write("\n".join(list))


def extract_basename(filename):
    tokens = filename.split('.')
    return tokens[0]


def main():
    forw = sys.argv[1]
    rev = sys.argv[2]

    counter1, counter2 = [0, 0]

    concat_reads = set()
    reads1, reads2 = dict(), dict()

    repeated_reads = dict()
    unpaired_reads_identifiers = list()

    forw_reads = fetch_seq_records(forw, 'fastq')

    for rec2 in SeqIO.parse(rev, extension(rev)):

        rec1 = forw_reads[rec2.name]

        if rec1:  # if there is one read from the forward file...

            seq1 = str(rec1.seq)
            seq2 = str(rec2.seq)
            concat = seq1 + seq2

            if concat not in concat_reads:
                concat_reads.add(concat)
                reads1[concat] = {'name': rec1.name, 'rec': rec1, 'sum_qual': sequence_quality_sum(rec1)}
                reads2[concat] = {'name': rec2.name, 'rec': rec2, 'sum_qual': sequence_quality_sum(rec2)}
            else:
                old_info1 = reads1[concat]
                old_info2 = reads2[concat]

                old_sum = old_info1['sum_qual'] + old_info2['sum_qual']

                current_sum = sequence_quality_sum(rec1) + sequence_quality_sum(rec2)

                # if the quality sum of these records is greater than the one already, replace it
                # with this one

                if (current_sum > old_sum):
                    print "Found a better repeated read!"
                    reads1[rec1.name] = {'name': rec1.name, 'rec': rec1, 'sum_qual': sequence_quality_sum(rec1)}
                    reads2[rec2.name] = {'name': rec2.name, 'rec': rec2, 'sum_qual': sequence_quality_sum(rec2)}

        else:  # this read does not pair  with any one on the forward reads file
            unpaired_reads_identifiers.append(rec2.name)

        counter1 += 1
        counter2 += 1

    print "Number of reads in forwards file " + forw + " :" + str(counter1)
    print "Number of reads in reverses file " + rev + " :" + str(counter2)
    print "Number of unique concat'ed reads : %d (%f%%)" % (
        len(concat_reads), len(concat_reads) / float(counter1) * 100.0)

    fastq_forwards_filename = basename(forw) + ".unique.fastq"
    fastq_reverses_filename = basename(rev) + ".unique.fastq"

    write_dict_fastq(fastq_forwards_filename, reads1)
    write_dict_fastq(fastq_reverses_filename, reads2)

    unpaired_ids_filename = basename(forw) + ".unpaired.txt"
    fasta_repeated_filename = basename(forw) + ".repeated.fasta"
    save_list(unpaired_ids_filename, unpaired_reads_identifiers)
    save_dict_fasta(fasta_repeated_filename, repeated_reads)

    print "The unique forward reads are stored in " + fastq_forwards_filename
    print "The unique reverse reads are stored in " + fastq_reverses_filename
    print "The unpaired read ids were stored in " + unpaired_ids_filename
    print "The repeated reads were stored in " + fasta_repeated_filename


if __name__ == '__main__':
    main()
