import sys
import smtplib as smtp


def main():
    # text = ''
    #
    # for line in sys.stdin:
    #     text += line

    msg = sys.argv[4]

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = sys.argv[3]
    msg['From'] = sys.argv[1]
    msg['To'] = sys.argv[2]

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtp.SMTP('mail.igc.gulbenkian.pt')

    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()


if __name__ == '__main__':
    main()
