from collections import OrderedDict
import sys

from orderedset._orderedset import OrderedSet

import commons


def get_named_item(list, names, name):
    pos = names.index(name)
    # if pos < 0: raise Exception("There is no element in list with that value!")
    return list[pos]


def main():
    diffs_table_filename = sys.argv[1]

    diffs_data, mergers_names = commons.tsv_fetch_rows_and_colnames(diffs_table_filename)

    print sys.argv[0], ": number of rows on ", diffs_table_filename, ":", len(diffs_data)

    commons.write_to_report([
        "On mismatches getter there were %d rows" % len(diffs_data)
    ])
    # item = diffs_data.popitem()

    # print get_named_item(item[1],mergers_names,'usearch')

    # create one file for each merger

    positives_for_merger = OrderedDict()
    wrongly_merged = OrderedDict()
    for name in mergers_names:
        positives_for_merger[name] = OrderedSet()
        wrongly_merged[name] = OrderedSet()


    i = 0

    for id, buckets in diffs_data.iteritems():
        for merger in mergers_names:
            value = get_named_item(buckets, mergers_names, merger)
            if value.strip() == '-': value = 0
            if int(value) < 0:
                positives_for_merger[merger].add(id)
            else:
                wrongly_merged[merger].add(id)

    token_file_name = commons.basename(diffs_table_filename)
    for merger in mergers_names:
        well_merged_accessions_filename = token_file_name + "_" + merger + ".txt"
        f = open(well_merged_accessions_filename, "w")
        f.write("\n".join(positives_for_merger[merger]))
        commons.write_to_report(["mismdiffcatcher: Wrote %d WELL merged accessions on %s " % (
        len(positives_for_merger[merger]), well_merged_accessions_filename)])

        wrongly_merged_accessions_filename = token_file_name + "_" + merger + ".wrong.txt"
        f = open(wrongly_merged_accessions_filename, "w")
        f.write("\n".join(wrongly_merged[merger]))
        commons.write_to_report(["mismdiffcatcher: Wrote %d  WRONGLY merged accessions on %s " % (
        len(wrongly_merged[merger]), wrongly_merged_accessions_filename)])

if __name__ == '__main__':
    main()
