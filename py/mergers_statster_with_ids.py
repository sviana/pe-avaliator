import argparse
import csv
import time
from collections import OrderedDict
import os
import traceback

import commons


def precision(tp, fp):
    if (tp + fp == 0):
        return 1.0 / float('inf')
    else:
        return (tp / float(tp + fp))


def recall(tp, fn):
    return (tp / float(tp + fn))


def fpr(tp, fp):
    return (fp / float(tp + fp))


positives_set = negatives_set = all_set = with_blast_set = None


def fmeasure(_precision, _recall):
    if (_precision + _recall == 0.0):
        return 1.0 / float('inf')
    else:
        return 2 * _precision * _recall / (_precision + _recall)


def analyze_merged_reads_file(merger_name, reads_ids_filenames, diff_accessions_filename):
    global positives_set, negatives_set, all_set, with_blast_set

    print "Fetching the whole merged read ids on file " + reads_ids_filenames
    merged_reads_list = commons.list_lines_from_file(reads_ids_filenames)

    merged_set = set(merged_reads_list)

    #    with_blast_merged_accessions_set = merged_accessions_set   & with_blast_reads_set

    if os.path.exists(diff_accessions_filename):
        print "Analysing the 'well merged' read ids on file " + diff_accessions_filename
    else:
        print "ERROR: File with the well merged does not exist!", diff_accessions_filename
        return {}

    well_merged_list = commons.list_lines_from_file(diff_accessions_filename)
    well_merged_set = set(well_merged_list)

    with_blast_well_merged_set = well_merged_set & with_blast_set

    without_blast_reads_set = all_set - with_blast_set

    well_merged_with_blast_set = well_merged_set & with_blast_set

    unmerged = all_set - merged_set

    unmerged_with_no_blast = unmerged - with_blast_set

    unmerged_with_blast = unmerged & with_blast_set

    merged_with_blast_set = merged_set & with_blast_set

    not_well_merged = merged_set - well_merged_set

    not_well_merged_with_blast = not_well_merged & with_blast_set

    true_negatives = unmerged_with_blast - positives_set

    true_positives = positives_set & well_merged_with_blast_set

    false_positives = merged_with_blast_set - true_positives

    false_negatives = unmerged_with_blast & positives_set


    stats_row = OrderedDict()

    # print "Total reads:", len (all_reads_set)

    _precision = precision(len(true_positives), len(false_positives))

    _recall = recall(len(true_positives), len(false_negatives))

    _fpr = fpr(len(true_positives), len(false_positives))

    _fmeasure = fmeasure(_precision, _recall)

    stats_row['name'] = merger_name

    stats_row['precision'] = "%.4f" % _precision

    stats_row['recall'] = "%.4f" % _recall

    stats_row['f_measure'] = "%.4f" % _fmeasure

    stats_row['fpr'] = "%.4f" % _fpr

    stats_row['total_merged'] = len(merged_with_blast_set)

    stats_row['not_merged'] = len(unmerged_with_blast)

    stats_row['well_merged'] = len(well_merged_with_blast_set)

    stats_row['not_well_merged'] = len(not_well_merged_with_blast)



    stats_row['true_positives'] = len(true_positives)

    stats_row['true_negatives'] = len(true_negatives)

    stats_row['false_positives'] = len(false_positives)

    stats_row['false_negatives'] = len(false_negatives)

    stats_row['all'] = len(well_merged_with_blast_set) + len(not_well_merged_with_blast) + len(unmerged_with_blast)

    stats_row['tp+tn+fp+fn'] = len(true_positives) + len(true_negatives) + len(false_positives) + len(false_negatives)

    # name, precision, recall, f_measure, fpr, total_merged, not_merged, true_positives, true_negatives, false_positives, false_negatives


    return stats_row


def main():
    argparser = argparse.ArgumentParser(
        description='generate the precision/recall metrics given a collection of accessions from the mergers')

    argparser.add_argument('--merged_accessions_dir',
                           help='the directory where the files with accessions from the merged reads are')
    argparser.add_argument('--merged_accessions_filenames_filter',
                           help='the regexp use to capture the accessions filenames')
    argparser.add_argument('--dataset_name', help='The dataset name')
    argparser.add_argument('--stats_filename',
                           help='The filename that should be used for the tsv where the metrics are saved')
    argparser.add_argument('--diff_accessions_dir', help='the dir containing the accessions with the favorable diffs')
    argparser.add_argument('--diff_accessions_filenames_filter',
                           help='the unix regexp used to capture the file with accessions with favorable diffs')

    args = argparser.parse_args()

    global positives_set, negatives_set, all_set, with_blast_set

    merged_reads_ids_files_dir = args.merged_accessions_dir

    merged_reads_ids_filenames_filter = args.merged_accessions_filenames_filter

    dataset_name = args.dataset_name

    stats_filename = args.stats_filename

    diff_accessions_dir = args.diff_accessions_dir

    diff_accessions_filenames_filter = args.diff_accessions_filenames_filter

    before = time.time()

    # these are the 'a-priori' positives (the ones which were determine have a potential overlap)

    print "Fetching the 'a priori positives' ..."

    pickled_dir = os.environ['PICKLED']

    positives_set = commons.unpickle_from_file(
        pickled_dir + "/" + dataset_name + '.positives-accessions.pickled')

    print "Fetching the 'a priori negatives' ... "

    negatives_set = set()

    negatives_path = pickled_dir + "/" + dataset_name + '.negatives-accessions.pickled'

    if (os.path.exists(negatives_path)):
        negatives_set = commons.unpickle_from_file(negatives_path)

    print "Fetching the unpaireds  ... "

    unaireds_accessions_set = set()

    unpaireds_path = pickled_dir + "/" + dataset_name + ".unpaireds-accessions.pickled"

    if (os.path.exists(unpaireds_path)):
        unpaireds_accessions_set = commons.unpickle_from_file(unpaireds_path)

    print "Fetching the missings  ... "

    missings_accessions_set = set()

    missings_path = pickled_dir + "/" + dataset_name + ".missings-accessions.pickled"
    if (os.path.exists(missings_path)):
        missings_accessions_set = commons.unpickle_from_file(missings_path)

    print "Fetching the mispaireds  ... "

    mispaireds_accessions_set = set()

    mispaireds_path = pickled_dir + "/" + dataset_name + ".mispaireds-accessions.pickled"

    if (os.path.exists(mispaireds_path)):
        mispaireds_accessions_set = commons.unpickle_from_file(mispaireds_path)



    # all reads

    print "Fetching all reads accessions  ... "

    all_set = commons.unpickle_from_file(pickled_dir + "/" + dataset_name + '.cleaned.pickled')

    # => simply remove ALL read ids that hadn't got any BLAST 
    with_blast_set = all_set - unpaireds_accessions_set - missings_accessions_set - mispaireds_accessions_set

    print "Reading of accessions done! Total time lasted: ", (time.time() - before)

    # these are the reads which were merged by some PE merger

    compiled_stats = OrderedDict()

    print "Listing all the " + merged_reads_ids_filenames_filter + " files on " + merged_reads_ids_files_dir

    staster_headers = ['name', 'precision', 'recall', 'f_measure', 'fpr', 'total_merged', 'not_merged', 'well_merged',
                       'not_well_merged', \
                       'true_positives', 'true_negatives', 'false_positives', 'false_negatives', 'all', 'tp+tn+fp+fn']

    staster_writer = csv.DictWriter(open(stats_filename, "w", buffering=0), staster_headers, delimiter="\t")

    staster_writer.writeheader()

    dataset_name_original = dataset_name

    dataset_name = dataset_name

    mergers_names = commons.list_files(merged_reads_ids_files_dir, merged_reads_ids_filenames_filter)

    mergers_names = sorted(mergers_names)

    for reads_ids_filename in mergers_names:
        merger_name = os.path.basename(reads_ids_filename) \
            .replace(dataset_name + '_MERGED.', '') \
            .replace('.fasta.ids.txt', '')


        merger_name_2 = merger_name.replace(dataset_name_original + '_MERGED.', '')

        diff_accessions_filename = diff_accessions_dir + os.sep + dataset_name_original + "-mismatches.diff_" + merger_name_2 + ".txt"

        try:
            compiled_stats[merger_name] = analyze_merged_reads_file(merger_name, reads_ids_filename,
                                                                    diff_accessions_filename)
        except Exception as ex:
            print "Something wrong happened when writing stats row about ", merger_name
            print ex
            traceback.print_exc()
            continue

        staster_writer.writerow(compiled_stats[merger_name])



    # staster_headers = commons.fetch_table_dict_headers(compiled_stats)


    # for merger in compiled_stats.keys():

    print "All the stats were written to " + stats_filename


if __name__ == '__main__':
    main()
