import xlwt
import os
import sys

import commons


def write_row(sheet, row_nr, row_data, is_num=True):
    for col_nr in range(0, len(row_data)):
        cell = row_data[col_nr]

        #        if not type(cell) == xlwt.Formula and cell.isdigit():
        #            cell = int(cell)
        #        elif is_num and  re.match(cell,'[0-9]+\.[0-9]+'):
        #            cell = float(cell)
        sheet.write(row_nr, col_nr, cell)


def main():
    tsv = sys.argv[1]

    xls = commons.basename(tsv) + ".xls"

    tsv_data, colnames = commons.tsv_fetch_rows_and_colnames(tsv, 'name', None, None, True)

    print colnames

    doc = xlwt.Workbook()

    sh = doc.add_sheet('precision_recall', True)

    # rows = sh.get_height()

    # print rows

    write_row(sh, 0, colnames, False)

    row_nr = 1

    for row_id in tsv_data.keys():
        row_data = tsv_data[row_id]

        # column 'tp+tn+fp+fn'


        r = row_nr + 1
        row_data[-1] = xlwt.Formula('J%d+K%d+L%d+M%d' % (r, r, r, r))
        # column 'all'
        row_data[-2] = xlwt.Formula('F' + str(row_nr + 1) + '+G' + str(row_nr + 1))

        write_row(sh, row_nr, row_data)
        row_nr += 1

    doc.save(xls)

    os.system('xdg-open ' + xls)


if __name__ == '__main__':
    main()
