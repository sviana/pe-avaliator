import sys

from Bio import SeqIO
import commons

__author__ = 'sam'


def common_reads():
    reads_filename1 = sys.argv[1]
    reads_filename2 = sys.argv[2]

    reads_final_filename = sys.argv[3]
    reads_final_file = open(reads_final_filename, "w")

    accessions1 = commons.fetch_accessions(reads_filename1, "fasta")
    accessions2 = commons.fetch_accessions(reads_filename2, "fasta")

    len_reads1 = len(accessions1)
    len_reads2 = len(accessions2)

    common_reads_keys = accessions1 & accessions2

    for seq_rec in SeqIO.parse(open(reads_filename1), "fasta"):
        if seq_rec.name in common_reads_keys:
            commons.write_seq_in_fasta_without_newlines(reads_final_file, seq_rec)


if __name__ == '__main__':
    common_reads()
