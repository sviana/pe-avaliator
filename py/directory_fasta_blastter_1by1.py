import argparse
import time

from commons import *
import commons

__author__ = 'sam'


# query id, subject id, % identity, alignment length, mismatches, gap opens, q. start,
# q. end, s. start, s. end, evalue, bit score, sequence

def insert_blastresults_empty_file_headers(filename):
    writer = csv.writer(open(filename, "w"), delimiter='\t')
    headers = ["id", "sid", "p_id", "a_len", "mism", "g_op", "qst", "qend", "sst", "send", "eval", "bitsc"]
    writer.writerow(headers)


def main():
    argparser = argparse.ArgumentParser(description='blast a dataset on a dir against the reference in another dir')

    argparser.add_argument('--source_dir', help='the dir with the FASTx to be blastted')
    argparser.add_argument('--seq_identity', help='threshold for sequence identity')
    argparser.add_argument('--ref_dir',
                           help='the dir with the reference to blast against, with one fasta for each organism')
    argparser.add_argument('--ref_type',
                           help='the reference format: raw FASTA files or BLASTDB'),
    argparser.add_argument('--accession_organism_table',
                           help='the  table containing the organisms against eash source sequence should be blastted against')
    argparser.add_argument('--blastresults_final_dir',
                           help='the dir where the file containing the BLAST results should be saved')
    argparser.add_argument('--temp_dir', help='dir to be used to store the temporary FASTAs from the source sequences ')
    args = argparser.parse_args()

    print sys.argv[0] + " : arguments: ", args

    fastq_source_dir_name = args.source_dir
    seq_identity = args.seq_identity

    fasta_organisms_dir = args.ref_dir
    accession_organism_tsv = args.accession_organism_table

    ref_type = args.ref_type

    if args.blastresults_final_dir:
        blastresults_final_dir = args.blastresults_final_dir
    else:
        blastresults_final_dir = fastq_source_dir_name

    temp_dir = args.temp_dir

    default_blast_args = {
        'perc_identity': float(seq_identity)
    }

    blast_args = default_blast_args


    if (os.environ.has_key('USE_MEGABLAST') and os.environ['USE_MEGABLAST'] == '1'):
        blast_args['task'] = 'megablast'

    before = time.time()

    fastq_on_dir = list_files(fastq_source_dir_name, "*.fastq")

    for fastq in fastq_on_dir:
        fasta_for_fastq = commons.basename(fastq) + ".fasta"
        if not os.path.exists(fasta_for_fastq):
            print "Not FASTA file for BLAST: creating a FASTA for ", fastq
            fasta_filename = commons.fastq_to_fasta(fastq)


    for fasta_filename in list_files(fastq_source_dir_name, "*.fasta"):
        blastresults_filename = basename(fasta_filename) + ".blastresults"


        print "=> Preparing for BLAST, using FASTA file ", fasta_filename
        print "=> The results will be saved to ", blastresults_filename

        blastresults_final_filename = blastresults_final_dir + os.path.sep + os.path.basename(blastresults_filename)

        if (os.path.exists(blastresults_final_filename) and os.path.getsize(blastresults_final_filename) > 0):
            print "%s were already processed!" % fasta_filename
            continue

        fasta = fasta_filename

        print "Number of records in %s: %d " % (fasta, count_lines(fasta) / 2)

        executing("touch %s" % (blastresults_filename))
        insert_blastresults_empty_file_headers(blastresults_filename)

        # = / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /
        run_blast_1by1_mp(fasta, fasta_organisms_dir, accession_organism_tsv, blastresults_filename, temp_dir, ref_type,
                       blast_args)
        # = / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /= / = /

        blastresults_lines = count_lines(blastresults_filename)
        print "Number of lines in %s: %s " % (blastresults_filename, blastresults_lines)
        print "Filesize: ", os.path.getsize(blastresults_filename)



    after = time.time()
    print "Took %f minutes! " % ((after - before) / 60.0)


if __name__ == '__main__':
    main()
