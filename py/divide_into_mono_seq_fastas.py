import os
import sys

from Bio import SeqIO

import commons


def fasta_mkblastdb(seq_rec, args):
    """"""
    if not args.has_key('target_dir'):
        print "write_single_seq_record(): target dir to write to not provided! aborting!"
        return

    target_dir = args['target_dir']

    # print seq_rec.name

    tokens = seq_rec.name.split('|')
    if (tokens[0] == 'lcl'):
        if len(tokens) == 2:
            seq_short_name = tokens[1]
        else:
            seq_short_name = tokens[2]

    seq_long_name = "|".join(tokens[:-1])


    seq_rec.name = seq_short_name
    seq_rec.id = seq_short_name
    seq_rec.description = seq_short_name

    #print "=>NEW SEQ_NAME:", seq_short_name

    fasta_name = target_dir + os.sep + seq_short_name + '.fasta'

    if fasta_name.find(':') != -1:
        fasta_name = fasta_name.replace(':', '_')

    SeqIO.write(seq_rec, fasta_name, 'fasta')

    blastdb_name = commons.basename(fasta_name) + ".blastdb"
    makeblastdb_cmdline = "makeblastdb -in %s -input_type fasta -dbtype nucl -parse_seqids -out %s" % \
                          (fasta_name, blastdb_name)
    commons.executing(makeblastdb_cmdline)



def main():
    fasta_to_divide_filename = sys.argv[1]
    target_dir = sys.argv[2]

    total_records = commons.fastx_count_records(fasta_to_divide_filename)

    print sys.argv[0] + " : are going to save the sequences in  " + fasta_to_divide_filename + " in their own FASTA"
    print "Target dir: ", target_dir
    print "Total records in source FASTA: ", total_records

    commons.walk_sequences(fasta_to_divide_filename, fasta_mkblastdb, target_dir=target_dir)


if __name__ == '__main__':
    main()
