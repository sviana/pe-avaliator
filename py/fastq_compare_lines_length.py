import sys


def main():
    fastq_name = sys.argv[1]

    i = 0

    for line in open(fastq_name):
        line = line.strip()
        i += 1
        if i % 4 == 1:
            last_accession = line
        elif i % 4 == 2:
            last_seq_line = line
        elif i % 4 == 0:
            last_qual_line = line

            if len(last_seq_line) != len(last_qual_line):
                print "Length differs on line %d, id %s" % (i, last_accession)


if __name__ == '__main__':
    main()
