import sys
import random
import uuid

from Bio import SeqIO

import commons


def randstringofsize(char_range, times):
    buf = ''
    for i in range(1, times+1):
        buf += random.choice(char_range)
    return buf


def generate_random_strings():
    word_range = 'abcdefghijklmnopqrstuvwxyz1234567890*_,;'
    f = open('randstrings.txt', 'w')
    for x in range(0, 1000):
        rand_string = randstringofsize(word_range, 20)
        f.write(rand_string + "\n")
    f.close()



def write_seq(target_handle,seq_rec):
    SeqIO.write(seq_rec, target_handle, 'fastq')


def get_last_random_char():
   global last_random_char
   return last_random_char

def get_last_new_key():
   global last_new_key
   return last_new_key

def new_random_char():
   global last_random_char
   last_random_char = randstringofsize('ABCDEFGHIJKLMNOPQRSTUVWXYZ',10)
#   print last_random_char
   return last_random_char

last_new_key = None

def main():
    global i
    global j

    forw_fastq = sys.argv[1]
    rev_fastq = sys.argv[2]

    forw_fastq_new_ids = open(commons.basename(forw_fastq) + ".new_ids.fastq", "w")
    rev_fastq_new_ids = open(commons.basename(rev_fastq) + ".new_ids.fastq", "w")

    print "Fetching records from ", forw_fastq, " and ", rev_fastq

    try:
        print "Reading forwards..."
        seqs_forw = SeqIO.index(forw_fastq, 'fastq')

        i = 0
        print "Reading reverses..."
        seqs_rev = SeqIO.index(rev_fastq, 'fastq')

        print "Writing new files with forwards and reverses..."

        j = 0

        k = 0
        #        for (rec_forw, rec_rev) in zip(seqs_forw.values(), seqs_rev.values())

        for rec_id in seqs_forw:
            rec_forw = seqs_forw[rec_id]
            rec_rev = seqs_rev[rec_id]
            if rec_forw.id == rec_rev.id:
                new_generated_key = new_key(rec_forw.id)
                rec_forw.id = new_generated_key
                rec_rev.id = new_generated_key
                SeqIO.write(rec_forw, forw_fastq_new_ids, 'fastq')
                SeqIO.write(rec_rev, rev_fastq_new_ids, 'fastq')
            else:
                print "Ids not equal!"

            k += 1
            if k % 10000 == 0: print k

    except Exception as ex:
        print ex


i = 0
k = 0

last_new_keys = []


def show_seeking(key):
    global i
    i += 1
    if i % 10000 == 0: print i
    return key


def get_last_key(orig_key):
    global j
    global last_new_keys
    j += 1
    if j % 10000 == 0: print j
    return last_new_keys[j - 1]


def new_key(orig_key):
    global k
    global last_new_keys
    var_portion = str(uuid.uuid1()).replace('-', '')[0:10]
    #    tokens = orig_key.split(':')
    #    var_portion = randstringofsize('1234567890',5) + ':' +  randstringofsize('1234567890',4)
#    last_new_key = ":".join(tokens[:-2]) + ':' + var_portion
    last_new_key = orig_key +  ':' + var_portion
    #print last_new_key
    #    k += 1
    #    if k % 10000 == 0: print k
    # last_new_keys.append(last_new_key)
    return last_new_key

def new_uuid(orig_key):

    global i
    i += 1
    last_new_key = str(uuid.uuid1())
    if i % 10000 == 0: print i
    return last_new_key


if __name__ == '__main__':
    main()
