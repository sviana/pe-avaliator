

import os,sys
import commons
from pandas import Series
from pandas import DataFrame

def main():
	fastq_name = sys.argv[1]

	scores = []

	i = 0
	rec = 0
	for line in open(fastq_name):
		i+=1
		line = line.strip()
		if (i%4 == 0):
			rec += 1
			score_for_line = grab_scores(line) 
#			print score_for_line
#			print "-"
			scores += score_for_line
			if (rec % 10000) == 0: print(rec)
	df = DataFrame(Series(scores))
	print df.describe()

def grab_scores(line):
	scores = map(lambda c: ord(c)-33,line)
	return scores

if __name__ == '__main__':
    main()
