import time

from pandas import DataFrame

from commons import *

__author__ = 'sam'


def fetch_accessions_trim_format(fasta_file, file_format):
    accessions = list()
    for seqRecord in SeqIO.parse(fasta_file, file_format):
        accessions.append(seqRecord.name)
    # print accessions

    print  "Number of reads in fasta " + fasta_file + ":" + str(len(accessions))

    size_megs = count_size_megs(accessions)

    # print "Total memory to allocate accessions: %f " % size_megs + " MB"

    return accessions


def get_proper_read_id(trimmed_read_id):
    return trimmed_read_id[0:trimmed_read_id.rfind(':')]


def fetch_fused_blastresults(blastresults_filename):
    # query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue,
    # bit score,
    blastres_numlines = count_lines(blastresults_filename)

    csvfile = open(blastresults_filename, "r")

    sniffer = csv.Sniffer()

    sample = csvfile.readline()

    csvfile.seek(0)

    blastres_reader = csv.reader(open(blastresults_filename, "r"), delimiter="\t")

    if (sniffer.has_header(sample)):
        blastres_reader.next()  # skips the header

    blastresults_dict = OrderedDict()

    i = 0

    before = time.time()

    for row in blastres_reader:
        id = row[0]
        seq_id = get_proper_read_id(id)
        direction = id.split(':')[-1]

        try:
            blastresults_dict[seq_id]
        except KeyError:
            blastresults_dict[seq_id] = dict()

        blastresults_dict[seq_id][direction] = {'length': int(row[3]), 'start': int(row[8]), 'end': int(row[9]),
                                                'organism': row[1]}

        i += 1

        if i % 100000 == 0:
            print "Total blastresults lines verified: %d (%f%%)" % (i, float(i) / float(blastres_numlines) * 100.0)

    mem_megs = count_size_megs(blastresults_dict)
    print "Total of memory allocated from " + blastresults_filename + " : " + str(mem_megs) + " MB"
    print "Time lasted since beginning: %d seconds" % int(time.time() - before)

    return blastresults_dict


def fetch_accessions(reads_file, format):
    accessions = OrderedSet()

    for seqRecord in SeqIO.parse(reads_file, format):
        accessions.add(get_proper_read_id(seqRecord.name))
    # print accessions

    print  "Number of accessions in " + reads_file + ":" + str(len(accessions))
    return accessions


def main():
    fused_blastresults_filename = sys.argv[1]
    fasta_ref_filename = sys.argv[2]

    overlap_basic_stats_filename = None

    if len(sys.argv) > 3:
        overlap_basic_stats_filename = sys.argv[3]



    #    seq_ids = fetch_accessions (fasta_ref_filename,"fasta" )

    blastres_dict = fetch_fused_blastresults(fused_blastresults_filename)

    positive_accessions = set()

    negatives = dict()

    positives = dict()

    all_overlaps = dict()

    unpaireds = set()

    before = time.time()

    counter_except = 0

    counter_mispaired = 0

    mispaired_accessions = set()

    hitted_organisms = dict()

    i = 0

    read_count = len(blastres_dict)

    print sys.argv[
              0] + ": peering through the 'analyzed blastresults' and detecting eventual positives or negatives for merging . . ."

    for accession in blastres_dict.keys():
        try:
            forw_data = blastres_dict[accession]['F']
            rev_data = blastres_dict[accession]['R']

            if (forw_data['organism'] != rev_data['organism']):
                print "Organism differs on the two paired-end reads: %s, %s" % (
                forw_data['organism'], rev_data['organism'])
                counter_mispaired += 1
                mispaired_accessions.add(accession)
                continue

            if (forw_data['start'] < rev_data['start']):
                overlap = forw_data['end'] - rev_data['end']
            else:
                overlap = rev_data['end'] - forw_data['end']

            start_min = min(forw_data['start'], rev_data['end'])
            start_max = max(forw_data['start'], rev_data['start'])

            diff_pos = start_max - start_min


            all_overlaps[accession] = overlap

            sum_lengths = forw_data['length'] + rev_data['length']

            detailed_info = {'id': accession, 'diff_pos': diff_pos, 'sum_lengths': sum_lengths,
                             'overlap': all_overlaps[accession],
                             'forw_start': forw_data['start'], 'forw_end': forw_data['end'],
                             'forw_length': forw_data['length'],
                             'rev_start': rev_data['start'], 'rev_end': rev_data['end'],
                             'rev_length': rev_data['length']
                             }
            hitted_organisms[accession] = forw_data['organism']

            if overlap > 10:
                positive_accessions.add(accession)
                positives[accession] = detailed_info
            else:
                negatives[accession] = detailed_info
        except KeyError:
            counter_except += 1
            unpaireds.add(accession)

        if i % 25000 == 0:
            print "Total accessions verified 'til now: %d (%f%%)" % (i, float(i) / read_count * 100.0)
        i += 1

    print "Total time lasted iterating through blast results: %d seconds" % int(time.time() - before)

    negatives_filename = basename(fused_blastresults_filename) + ".negatives_a_priori.tsv"

    apriori_candidates_file_header = ['id', 'diff_pos', 'sum_lengths', 'overlap', 'forw_start', 'forw_end',
                                      'forw_length', 'rev_start',
                                  'rev_end', 'rev_length']
    negatives_writer = csv.DictWriter(open(negatives_filename, "w"), apriori_candidates_file_header,
                                           delimiter='\t')

    print "Writing the true negatives detailed info to file ..."

    negatives_writer.writeheader()

    for accession in sorted(negatives.keys()):
        data = negatives[accession]
        negatives_writer.writerow(data)

    positives_filename = basename(fused_blastresults_filename) + ".positives_a_priori.tsv"

    positives_writer = csv.DictWriter(open(positives_filename, "w"), apriori_candidates_file_header,
                                           delimiter='\t')

    positives_writer.writeheader()

    for accession in sorted(positives.keys()):
        data = positives[accession]
        positives_writer.writerow(data)

    print "Writing a file with only IDs and overlaps..."

    overlaps_filename = basename(fused_blastresults_filename) + ".overlaps.tsv"

    overlaps_writer = csv.DictWriter(open(overlaps_filename, "w"), ['accession', 'overlap'], delimiter='\t')
    overlaps_writer.writeheader()

    # seq_records = fetch_seq_records_trim_format(fasta_ref_filename,"fasta")

    fasta_presuntive_overlapped_filename = basename(fasta_ref_filename) + ".overlapps." \
                                           + extension(fasta_ref_filename)

    fasta_presuntive_overlapped = open(fasta_presuntive_overlapped_filename, "w")

    print "Writing a two-cols file with IDs and overlap length . . ."
    for accession in sorted(all_overlaps.keys()):
        overlaps_writer.writerow({'overlap': all_overlaps[accession], 'accession': accession})
        # write_seq_in_fasta_without_newlines( fasta_presuntive_overlapped, seq_records[accession])

    unpaireds_filename = basename(fused_blastresults_filename) + ".unpaireds.txt"

    unpaireds_file = open(unpaireds_filename, "w")

    unpaireds_file.write("\n".join(unpaireds))

    mispaireds_filename = basename(fused_blastresults_filename) + ".mispaireds.txt"

    write_list_to_textfile(mispaired_accessions, mispaireds_filename)

    hitted_organisms_filename = basename(fused_blastresults_filename) + ".organisms.tsv"

    hitted_organisms_file = open(hitted_organisms_filename, "w")

    for accession in hitted_organisms.keys():
        hitted_organisms_file.write(accession + "\t" + hitted_organisms[accession] + "\n")

    df = DataFrame.from_dict(all_overlaps, 'index')

    report_lines = ["Count true positive accessions : %d" % len(positive_accessions),
                    "Count true negative  accessions: %d" % len(negatives),
                    "unpaired accessions: %d" % counter_except,
                    "Mispaired accessions: %d" % counter_mispaired,
                    "Basic stats about overlaps (including negatives):",
                    str(df.describe())
                    ]

    print report_lines

    if (overlap_basic_stats_filename is not None):
        open(overlap_basic_stats_filename, "w").write(str(df.describe()))
        print "The statistics about overlaps were written to " + overlap_basic_stats_filename

    print "The a priori true negatives were stored in " + negatives_filename
    print "The a priori true positives were stored in " + positives_filename

    print "The overlaps were stored in " + overlaps_filename
    print "The unpaireds are on " + unpaireds_filename
    print "and the mispaireds on " + mispaireds_filename

    print "The hits sequence/organism are stored in " + hitted_organisms_filename

    #   print "A new FASTA file with the presuntive true positives were written to " + fasta_presuntive_overlapped_filename

    write_to_report(
        report_lines
    )

if __name__ == '__main__':
    main()
