#!/usr/bin/env python
import ConfigParser as cp
import re
import time
import argparse

import datetime
from commons import *
import get_config

global use_how_many_cores
overlaps_stats = None


def get_overlap_stats(overlap_stats_file):
    global overlaps_stats
    with open(overlap_stats_file, "r") as f:
        for line in f:
            line = line.strip()
            tokens = re.split('\W+', line, 2)
            if len(tokens) > 1:
                (param, value) = tokens[0:2]
                overlaps_stats[param] = value
    return overlaps_stats

def replace_placeholders(cmdline, forw, rev, out, merged):
    global use_how_many_cores

    cmdline = cmdline.replace('$cpus', use_how_many_cores)
    #    cmdline = cmdline.replace('$min_overlap', overlaps_stats['min'])
    cmdline = cmdline.replace('$max_overlap', overlaps_stats['max'])

    cmdline = cmdline.replace('%forward', forw)
    cmdline = cmdline.replace('%reverse', rev)
    cmdline = cmdline.replace('%out', out)
    outbasename = basename(out)
    forwardbasename = basename(forw)
    mergedbasename = basename(merged)
    cmdline = cmdline.replace('%oubase', outbasename)
    cmdline = cmdline.replace('%forwbase', forwardbasename)
    cmdline = cmdline.replace('%merged', merged)
    cmdline = cmdline.replace('%mergebase', mergedbasename)
    return cmdline


def main():
    argparser = argparse.ArgumentParser(description='Executes a batch of programs')

    argparser.add_argument('forward', help='the fastq file with the FORWARD paired-end read files')
    argparser.add_argument('reverse', help='the fastq file with the REVERSE paired-end read files')
    argparser.add_argument('out', help='the name to give to the FASTQ file with the merged paired-end read files')

    argparser.add_argument('--fastqc_original_files', help='apply fastqc over the original paired-end read files',
                           action='store_true')

    argparser.add_argument('--conf', help='If you need to specify another .ini configuration file')

    argparser.add_argument('--name', help='Give a name to the execution times log')

    argparser.add_argument('--num-cores', help="number of cores to use")

    argparser.add_argument('--overlap_stats_file', help='text stats about overscores to use')

    args = argparser.parse_args()

    forw = args.forward
    rev = args.reverse
    out = args.out

    global use_how_many_cores

    if args.num_cores:
        use_how_many_cores = args.num_cores
    else:
        use_how_many_cores = get_config.use_how_many_cpus()

    print "=== STARTING MERGERS TESTING ==="

    print "Using %s CPU cores " % (use_how_many_cores)

    print  "FASTQ WITH FORWARD READS: %s" % (forw)
    print  "    \"    \" REVERSE \"    : %s " % (rev)

    # exec fastQC on the forward and reverse paired ends reads files

    # if args.fastqc_original_files:
    #     print "Executing fastQC on FORwARD paired-end reads  file..."
    #     run_fastqc(forw )
    #     print "Executing fastQC on REVERSE paired-end reads REVERSE file..."
    #     run_fastqc(rev)

    config = cp.ConfigParser()

    # the 'by-default' config file
    config_file = 'lindy_hop.ini'

    if args.conf:
        config_file = args.conf

    print os.getcwd()
    config.readfp(open(config_file))

    execution_times = {}

    global overlaps_stats

    overlaps_stats = {}

    if args.overlap_stats_file:
        get_overlap_stats(args.overlap_stats_file)

    # tsv_regexp = out + '\-exec_times\.([0-9]+)\.tsv'
    #
    # last_tsv = [f for f in sorted(os.listdir('.')) if re.match(tsv_regexp, f)]

    #    exec_number = len(last_tsv) + 1

    formatted_datetime = datetime.datetime.now().strftime('%Y%m%d-%H%M')

    tsv_exec_times_filename = out + '-exec_times.' + formatted_datetime + '.csv'

    tsv_times = csv.DictWriter(open(tsv_exec_times_filename, 'w'), ['program', 'time'], delimiter="\t")
    tsv_times.writeheader()

    # for each section in the .ini file ...

    for section in config.sections():

        # adds an extension to the output file so that we now which program
        # is the responsable for that output (

        out_filename = out + "." + section

        # in the case of PEAR, fastq-join or mothur,
        # don't add fastq extension, since the application already does it
        if section.find('pear') < 0 \
                and section.find('fastq-join') < 0 \
                and section.find('mothur') < 0:
            out_filename += ".fastq"

        # some programs have their own way of naming the merged reads file,
        # so we have to deal with that !
        merged_reads_filename = out_filename
        if (section.find('fastq-join') >= 0):
            merged_reads_filename = out_filename + 'join'
        elif (section.find('pear') >= 0):
            merged_reads_filename = out_filename + '.assembled.fastq'
            merged_reads_filename = out_filename + '.fastq'
        elif (section.find('mothur') >= 0):
            merged_reads_filename = out_filename + ".fasta"

        # verify it there is also a merged reads file for this section, if there is,
        # skip it


        if os.path.isfile(merged_reads_filename) == True:
            print "Skipping ", section
            continue
        else:
            if section.find('fastq-join') >= 0:
                if os.path.isfile(out_filename + ".fastq") == True:
                    print "Skipping ", section
                    continue

        cmdline = config.get(section, 'cmdline')
        cmdline = replace_placeholders(cmdline, forw, rev, out_filename, merged_reads_filename)

        print ("-------------------------\nExecuting %s\n-------------------------" % section)
        print ("Command line : \n %s" % cmdline)

        before = time.time()

        executing(cmdline)

        after = time.time()

        total_execution_time = after - before

        if config.has_option(section, 'after'):
            after_option = config.get(section, 'after')
            after_option = replace_placeholders(after_option, forw, rev, out_filename, merged_reads_filename)
            executing(after_option)

        execution_times[section] = total_execution_time

        tsv_times.writerow({'program': section, 'time': total_execution_time})

        print "Total time lasted in the execution of %s : %f secs!" % (section, total_execution_time)

        # run fastqc with the file of merged reads


        # print "Running FastQC with the results file: %s " % (merged_reads_filename)

        # run_fastqc(merged_reads_filename)

    print "The execution times were stored on " + tsv_exec_times_filename


def run_fastqc(file):
    os.system("fastqc --extract -o . -t 4 %s" % (file))


def basename(filename):
    filename_tokens = filename.split('.')
    fasta_basename = ".".join(filename_tokens[0:len(filename_tokens) - 1])
    return fasta_basename


def fastq_to_fasta(fastq_filename):
    os.system('fastq_to_fasta -i ' + fastq_filename + ' -o %s -n' % (basename(fastq_filename)))


if __name__ == '__main__':
    main()
