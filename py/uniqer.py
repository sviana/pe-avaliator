from commons import *

__author__ = 'sam'


# read one FASTQ entirely into memory in the form a dictionary, using the name (identifier) as the key, and the sequence
# data as the value
# uses BioPython to parse the SeqRecords
# in the end writes the unique reads into two fasta files

def save_dict_fasta(filename, fasta_dict):
    f = open(filename, "w")
    for (key, value) in fasta_dict.iteritems():
        f.write(">" + key + "\n" + value + "\n")


def save_list(filename, list):
    f = open(filename, "w")
    f.write("\n".join(list))


def extract_basename(filename):
    tokens = filename.split('.')
    return tokens[0]


def main():
    forw = sys.argv[1]
    rev = sys.argv[2]

    counter1, counter2 = [0, 0]

    concat_reads = set()
    unique_reads1 = list()
    unique_reads2 = list()
    repeated_reads = dict()
    unpaired_reads_identifiers = list()

    forw_reads = fetch_sequences_into_ordered_dict(forw)

    for rec2 in SeqIO.parse(rev, extension(rev)):

        paired_read = forw_reads[rec2.name]

        if paired_read:  # if there is one read from the forward file...

            seq1 = paired_read
            seq2 = str(rec2.seq)
            concat = seq1 + seq2

            if concat not in concat_reads:
                concat_reads.add(concat)
                unique_reads1.append({'name': rec2.name, 'data': seq1})
                unique_reads2.append({'name': rec2.name, 'data': seq2})
            else:
                repeated_reads[rec2.name] = concat
        else:  # this read does not pair  with any one on the forward reads file
            unpaired_reads_identifiers.append(rec2.name)

        counter1 += 1
        counter2 += 1

    print "Number of reads in forwards file " + forw + " :" + str(counter1)
    print "Number of reads in reverses file " + rev + " :" + str(counter2)

    report_line = "Number of unique concat'ed reads : %d (%f%%)" % (
        len(concat_reads), len(concat_reads) / float(counter1) * 100.0)

    print report_line

    write_to_report([
        report_line
    ])

    fasta_forwards_filename = basename(forw) + ".unique.fasta"
    fasta_reverses_filename = basename(rev) + ".unique.fasta"

    write_list_fasta(fasta_forwards_filename, unique_reads1)
    write_list_fasta(fasta_reverses_filename, unique_reads2)

    unpaired_ids_filename = basename(forw) + ".unpaired.txt"
    fasta_repeated_filename = basename(forw) + ".repeated.fasta"
    save_list(unpaired_ids_filename, unpaired_reads_identifiers)
    save_dict_fasta(fasta_repeated_filename, repeated_reads)

    print "The unique forward reads are stored in " + fasta_forwards_filename
    print "The unique reverse reads are stored in " + fasta_reverses_filename
    print "The unpaired read ids were stored in " + unpaired_ids_filename
    print "The repeated reads were stored in " + fasta_repeated_filename


if __name__ == '__main__':
    main()
