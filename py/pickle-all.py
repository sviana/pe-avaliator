import sys

import commons


def main():
    for filename in sys.argv[1:]:
        basenam = commons.basename(filename)
        ext = commons.extension(filename)
        ids = set()
        if ext == 'tsv' or ext == 'txt':
            rows = commons.tsv_fetch_rows(filename)
            ids = set(rows.keys())
        elif ext.find('fast') >= 0:
            accessions = commons.fetch_accessions(filename)
            ids = set(accessions)
        pickled_filename = basenam + ".pickled"
        commons.pickle_to_file(ids, pickled_filename)
        report_line = "Pickled " + str(len(ids)) + " records to " + pickled_filename
        print report_line
        commons.write_to_report([report_line])


if __name__ == '__main__':
    main()
