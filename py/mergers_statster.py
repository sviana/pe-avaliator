import csv
import sys
import time
from collections import OrderedDict
import os

import commons


def precision(tp, fp):
    if (tp + fp == 0):
        return 1.0 / float('inf')
    else:
        return (tp / float(tp + fp))


def recall(tp, fn):
    return (tp / float(tp + fn))


def fpr(tp, fp):
    return (fp / float(tp + fp))


positives_set = negatives_set = all_set = None


def fmeasure(_precision, _recall):
    if (_precision + _recall == 0.0):
        return 1.0 / float('inf')
    else:
        return 2 * _precision * _recall / (_precision + _recall)


positives_set = negatives_set = all_set = None


def main():
    merged_reads_files_dir = sys.argv[1]

    merged_reads_filesfilter = sys.argv[2]

    staster_filename = sys.argv[3]

    pickled_dir = sys.argv[4]

    dataset_name = sys.argv[5]

    global positives_set, negatives_set, all_set

    before = time.time()


    # these are the 'a-priori' positives (the ones which were determine have a potential overlap)

    print "Fetching the 'a priori positives' ..."

    positives_accessions_set = commons.unpickle_from_file(
        pickled_dir + '/' + dataset_name + '.positives-accessions.pickled')

    print "Fetching the 'a priori negatives' ... "

    negatives_accessions_set = commons.unpickle_from_file(
        pickled_dir + '/' + dataset_name + '.negatives-accessions.pickled')

    # all reads

    print "Fetching all reads accessions  ... "

    all_reads_set = commons.unpickle_from_file(pickled_dir + "/" + dataset_name + '.cleaned.pickled')

    print "Reading of accessions done! Total time lasted: ", (time.time() - before)



    # these are the reads which were merged by some PE merger

    compiled_stats = OrderedDict()

    print "Listing all the  " + merged_reads_filesfilter + " files on " + merged_reads_files_dir

    for fastx_file in commons.list_files(merged_reads_files_dir, merged_reads_filesfilter):
        compiled_stats[os.path.basename(fastx_file)] = analyze_merged_reads_file(fastx_file)


    staster_headers = commons.fetch_table_dict_headers(compiled_stats)

    staster_writer = csv.DictWriter(open(staster_filename, "w"), staster_headers, delimiter="\t")

    staster_writer.writeheader()

    for merger in compiled_stats.keys():
        staster_writer.writerow(compiled_stats[merger])


if __name__ == '__main__':
    main()
