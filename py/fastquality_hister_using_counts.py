from math import log
import os
import sys
import collections
import csv
from time import time

import commons


def median(list):
    return list[len(list) / 2]


def avg(list):
    return float(sum(list)) / len(list)


def counts(list):
    return collections.Counter(list)


def counter_freq(counter):
    return map(lambda key: (key, counter[key]), counter.keys())


def counter_cumulatives(counter):
    freqs = counter_freq(counter)
    aux = 0
    cums = collections.OrderedDict()
    for (val, freq) in freqs:
        aux += val * freq
        cums[val] = aux
    return cums


def counter_median(counter):
    freqs = counter_freq(counter)
    val_sum = sum(counter.values())
    cums = counter_cumulatives(counter)

    half_dim = cums.popitem()[1] / 2
    # print "half dim", half_dim
    values = cums.keys()
    i = 0
    aux = cums[values[i]]
    while (aux < half_dim):
        i += 1
        if i >= len(cums):
            break
        next_val = values[i]
        aux = cums[next_val]
    return next_val


def counter_avg(counter):
    freqs = counter_freq(counter)
    aux = 0.0
    for (val, freq) in freqs:
        aux += val * freq
    return aux / sum(counter.values())


def counter_plot_hist(counter, fastq_name):
    import matplotlib.pyplot as pyplot
    pyplot.bar(counter.keys(), [log(val, 10.0) for val in counter.values()])

    pyplot.title(os.path.basename(fastq_name) + " SCORES HISTORGRAM")
    pyplot.xlabel("Values")
    pyplot.ylabel("Frequencies (10^x)")

    #   pyplot.show()

    fig = pyplot.gcf()

    histogram_image_name = commons.basename(fastq_name) + '.qual.histogram.png'

    fig.savefig(histogram_image_name)

    return histogram_image_name


def printsidebyside(list_tuples):
    for tuple in list_tuples:
        print tuple


def main():
    start = time()
    fastq_name = sys.argv[1]
    fastq_loc = os.path.dirname(fastq_name)

    if len(sys.argv) > 2:
        queue_size = int(sys.argv[2])
    else:
        queue_size = 10000

    print  "Using ", queue_size, " as queue size"

    score_counts = commons.fastq_scores_mp(fastq_name, queue_size)

    print "------"

    print os.path.basename(fastq_name)

    print "median", counter_median(score_counts)

    print "average", counter_avg(score_counts)

    tuple_freqs = [(val, score_counts[val]) for val in score_counts]

    print tuple_freqs

    freqs_tsv_name = commons.basename(fastq_name) + '.freqs.tsv'

    writer = csv.writer(open(freqs_tsv_name, 'w'), delimiter='\t')

    for freq in tuple_freqs:
        writer.writerow(freq)


    #    histogram_name = counter_plot_hist(score_counts, fastq_name)

    #    print "The histogram is ", histogram_name

    print "The frequencies for scores are in ", freqs_tsv_name

    #   os.system("xdg-open " + histogram_name)

    end = time()

    print "Took ", (end - start), " seconds! "



if __name__ == '__main__':
    main()
