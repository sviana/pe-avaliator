import sys

import commons


def main():
    filenames = sys.argv[1:]
    for filename in filenames:
        lines = open(filename, "r").readlines()
        lines = map(lambda x: x.strip('\n'), lines)
        filename_with_the_pickled = commons.basename(filename) + ".pickled"
        commons.pickle_to_file(lines, filename_with_the_pickled)

        print "Pickled %d to %s" % (len(lines), filename_with_the_pickled)


if __name__ == '__main__':
    main()
