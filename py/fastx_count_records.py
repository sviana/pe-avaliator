import sys

import commons


def main():
    fastx_filename = sys.argv[1]
    count = commons.fastx_count_records(fastx_filename)
    print "File ", fastx_filename, " has ", count , " records"

if __name__ == '__main__':
    main()
