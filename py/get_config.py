import os

import commons


def use_how_many_cpus():
    use_how_many_cores = None
    if os.environ.has_key('USE_CORES'):
        use_how_many_cores = os.environ['USE_CORES']
    else:
        use_how_many_cores = str(commons.num_cpus())
    return use_how_many_cores
