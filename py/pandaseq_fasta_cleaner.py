import sys

import Bio
import commons


def main():
    panda_fasta_file = sys.argv[1]

    cleaned_panda_fasta_filename = commons.basename(panda_fasta_file) + ".cleaned.fastq"

    cleaned_panda_fasta = open(cleaned_panda_fasta_filename, "w")

    for rec in Bio.SeqIO.parse(panda_fasta_file, 'fastq'):
        tokens = rec.id.split(':')
        new_id = ":".join(tokens[:-1])
        rec.id = new_id
        rec.description = new_id
        Bio.SeqIO.write(rec, cleaned_panda_fasta, "fastq")


if __name__ == '__main__':
    main()
