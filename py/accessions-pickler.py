import sys
import time
import pickle

import commons


def main():
    positives_filename = sys.argv[1]
    negatives_filename = sys.argv[2]

    all_reads_filename = sys.argv[3]

    missings_filename = sys.argv[4]


    # these are the reads which were merged by some PE merger

    before = time.time()

    # these are the 'a-priori' positives (the ones which were determine have a potential overlap)

    print "Fetching the 'a priori positives' from " + positives_filename

    positives = commons.tsv_fetch_rows(positives_filename)

    positives_accessions_set = set(positives.keys())

    pickle.dump(positives_accessions_set, open('positive-accessions.min-overlap10.pickled', "wb"))

    print "Fetching the 'a priori negatives' from " + negatives_filename

    negatives = commons.tsv_fetch_rows(negatives_filename)

    negatives_accessions_set = set(negatives.keys())

    pickle.dump(negatives_accessions_set, open('negative-accessions.min-overlap10.pickled', "wb"))

    # all reads

    print "Fetching all reads accessions from " + all_reads_filename

    all_reads_accessions = commons.fetch_accessions(all_reads_filename, "fastq")

    all_reads_set = set(all_reads_accessions)

    pickle.dump(all_reads_set, open('all-accessions.pickled', 'wb'))

    # missings

    print "Fetching the missings accessions from " + missings_filename

    missings_accessions = commons.tsv_fetch_rows(missings_filename)

    missings_set = set(all_reads_accessions)

    pickle.dump(missings_set, open('missings-accessions.min-overlap10.pickled', 'wb'))

    print "Pickling of accessions done! Total time lasted: ", (time.time() - before)


if __name__ == '__main__':
    main()
