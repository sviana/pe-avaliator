import sys

from Bio import SeqIO


def main():
    for seq_rec in SeqIO.parse(sys.argv[1], 'fastq'):
        print seq_rec


if __name__ == '__main__':
    main()
