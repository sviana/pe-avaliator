import os
import sys

import commons


def main():
    blastresults_filename = sys.argv[1]
    new_blastdb_name = sys.argv[2]
    target_dir = sys.argv[3]
    blast_organisms_ids_filename = commons.basename(blastresults_filename) + ".hits.ids.txt"
    blast_hits_ids = commons.tsv_fetch_rows(blastresults_filename, 1)
    print "Number of organisms found by BLAST:", len(blast_hits_ids)
    report_line = "Number of unique organisms found by BLAST: %d" % len(set(blast_hits_ids.keys()))
    print report_line
    commons.write_to_report([report_line])
    commons.write_list_to_textfile(blast_hits_ids, blast_organisms_ids_filename)

    blastdb = None

    if os.environ.has_key('BLASTDB'):
        blastdb = os.environ['BLASTDB']
    else:
        print "No blast database provided! Aborting!"
        sys.exit(-1)

    print sys.argv[0] + " : are going to subset a database from " + blastdb

    blastdb_subset_fasta_filename = target_dir + "/" + os.path.basename(blastdb) + "-" + new_blastdb_name + ".fasta"

    blastdb_subset_filename = commons.basename(blastdb_subset_fasta_filename) + ".blastdb"

    blastdbcmd_cmdline = "blastdbcmd -db %s -dbtype nucl -entry_batch %s -outfmt %%f -out %s" % \
                         (blastdb, blast_organisms_ids_filename, blastdb_subset_fasta_filename)
    commons.executing(blastdbcmd_cmdline)

    makeblastdb_cmdline = "makeblastdb -in %s -input_type fasta -dbtype nucl -parse_seqids -out %s" % \
                          (blastdb_subset_fasta_filename, blastdb_subset_filename)
    commons.executing(makeblastdb_cmdline)

    print "The new BLAST database with only the hits found were written to", blastdb_subset_filename


if __name__ == '__main__':
    main()
