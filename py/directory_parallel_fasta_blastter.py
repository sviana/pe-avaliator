import time

import get_config
from commons import *

__author__ = 'sam'


# query id, subject id, % identity, alignment length, mismatches, gap opens, q. start,
# q. end, s. start, s. end, evalue, bit score, sequence

def insert_blastresults_empty_file_headers(filename):
    writer = csv.writer(open(filename, "w"), delimiter='\t')
    headers = ["id", "sid", "p_id", "a_len", "mism", "g_op", "qst", "qend", "sst", "send", "eval", "bitsc"]
    writer.writerow(headers)





def main():
    fastq_source_dir_name = sys.argv[1]
    seq_identity = sys.argv[2]

    if len(sys.argv) >= 3:
        max_target_seqs = sys.argv[3]
    if len(sys.argv) >= 4:
        blastresults_final_dir = sys.argv[4]
    else:
        blastresults_final_dir = fastq_source_dir_name

    use_parallel = False

    if (os.environ.has_key('USE_PARALLEL') and os.environ['USE_PARALLEL'] == '1'):
        use_parallel = True

    cpu_cores = int(get_config.use_how_many_cpus())

    blastdb = os.environ['BLASTDB']

    default_blast_args = {
        'perc_identity': float(seq_identity),
        'db': blastdb,
        'num_threads': int(cpu_cores)
    }

    blast_args = default_blast_args

    if (os.environ.has_key('USE_PARALLEL') and os.environ['USE_PARALLEL'] == '1'):
        use_parallel = True

    if (os.environ.has_key('USE_MEGABLAST') and os.environ['USE_MEGABLAST'] == '1'):
        blast_args['task'] = 'megablast'





    before = time.time()

    for fastx_filename in list_files(fastq_source_dir_name, "*.fast?"):

        blastresults_filename = basename(fastx_filename) + ".blastresults"

        if (fastx_filename.find('.fastq') >= 0):
            new_fasta_filename = basename(fastx_filename) + ".fasta"
            if (not os.path.exists(new_fasta_filename)):
                fasta = fastq_to_fasta_p(fastx_filename)
            else:
                print new_fasta_filename + " already exists!"
                fasta = new_fasta_filename
        else:
            fasta = fastx_filename

        print "=> Preparing for BLAST, using FASTA file ", fasta
        print "=> The results will be saved to ", blastresults_filename

        br_filename = os.path.basename(blastresults_filename)

        blastresults_final_path = blastresults_final_dir + os.path.sep + os.path.basename(blastresults_filename)

        if (os.path.exists(blastresults_final_path)):
            print "%s were already processed!" % fastx_filename
            continue


        print "Number of records in %s: %d " % (fasta, count_lines(fasta) / 2)

        executing("touch %s" % (blastresults_filename))
        insert_blastresults_empty_file_headers(blastresults_filename)


        if use_parallel:
            if blast_args.has_key('num_threads'):
                del (blast_args['num_threads'])
            run_blast_parallel(fasta, blastresults_filename, blast_args)
        else:
            run_blast(fasta, blastresults_filename, blast_args)



        print "Number of lines in %s: %s " % (blastresults_filename, (count_lines(blastresults_filename)))
        print "Filesize: ", os.path.getsize(blastresults_filename) / 1024 ** 2, " MB"


        # break

    after = time.time()
    print "Took %f minutes! " % ((after - before) / 60.0)


if __name__ == '__main__':
    main()
