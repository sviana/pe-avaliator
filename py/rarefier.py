import sys
import random

from Bio import SeqIO

import commons


def randstringofsize(char_range, times):
    buf = ''
    for i in range(1, times):
        buf += random.choice(char_range)
    return buf


def generate_random_strings():
    word_range = 'abcdefghijklmnopqrstuvwxyz1234567890*_,;'
    f = open('randstrings.txt', 'w')
    for x in range(0, 1000):
        rand_string = randstringofsize(word_range, 20)
        f.write(rand_string + "\n")
    f.close()


def rarify(seq_rec, params):
    seq = str(seq_rec)
    target_handle = params['target_handle']
    rarify_factor = params['rarify_factor']
    format = params['format']
    rand = random.random()
    if (rand < (1.0 / rarify_factor)):
        SeqIO.write(seq_rec, target_handle, 'fastq')

def main():
    # generate_random_strings()

    # sys.exit(-1)

    to_be_rarified_filename = sys.argv[1]
    rarified_to_size_filename = sys.argv[2]

    format = commons.extension(to_be_rarified_filename)

    rarified_filename = commons.basename(to_be_rarified_filename) + '.rarified.' + format

    to_be_rarified_count = commons.fastx_count_records(to_be_rarified_filename)
    rarified_to_size_count = commons.fastx_count_records(rarified_to_size_filename)

    print "To be rarify count: %d" % (to_be_rarified_count)

    print "Rarified to count: %d" % (rarified_to_size_count)

    rarify_factor = float(to_be_rarified_count) / float(rarified_to_size_count)

    print "Rarify factor: %f" % (rarify_factor)

    source = open(to_be_rarified_filename, 'r')
    target = open(rarified_filename, 'w')

    commons.walk_sequences(to_be_rarified_filename, rarify, target_handle=target, rarify_factor=rarify_factor,
                           format=format)

    target.close()

    print "Final sequences file is " + rarified_filename

    print "Final sized of sequences file rarified: %d" % (commons.fastx_count_records(rarified_filename, format))

if __name__ == '__main__':
    main()
