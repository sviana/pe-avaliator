import sys

import commons


def fuse_paired_end_files():
    forw_reads_filename = sys.argv[1]
    rev_reads_filename = sys.argv[2]

    fused_reads_filename = sys.argv[3]

    # fused_reads_filename = commons.basename(forw_reads_filename) + ".fused." + commons.extension(forw_reads_filename)


    forw_reads_file = open(forw_reads_filename, "r")
    rev_reads_file = open(rev_reads_filename, "r")
    fused_reads_file = open(fused_reads_filename, "w")

    while True:

        line_forw = forw_reads_file.readline()
        line_rev = rev_reads_file.readline()

        if line_forw == '' or line_rev == '':
            break

        accession_forw = line_forw.lstrip('>').strip('\n')
        accession_rev = line_rev.lstrip('>').strip('\n')
        accession_fuse_forw = accession_forw + ":F"
        accession_fuse_rev = accession_rev + ":R"

        seq_forw = forw_reads_file.readline().strip('\n')
        seq_rev = rev_reads_file.readline().strip('\n')

        commons.write_bare_fasta_without_newlines(fused_reads_file, accession_fuse_forw, seq_forw)
        commons.write_bare_fasta_without_newlines(fused_reads_file, accession_fuse_rev, seq_rev)


if __name__ == '__main__':
    fuse_paired_end_files()
