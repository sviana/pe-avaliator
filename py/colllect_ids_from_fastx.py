import sys

import commons


def main():
    fastx_filename = sys.argv[1]
    ids_filename = fastx_filename + ".ids.txt"

    reads_ids = commons.fetch_accessions(fastx_filename)

    print "Writing %d read IDs to %s" % (len(reads_ids), ids_filename)

    commons.write_list_to_textfile(reads_ids, ids_filename)


if __name__ == '__main__':
    main()
