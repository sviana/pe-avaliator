from collections import Counter
import sys
import re

from Bio import SeqIO
from Bio.Seq import Seq

from commons import basename, extension
import commons

__author__ = 'sam'


def countBases(seq):
    counter = Counter()
    for b in seq:
        counter[b] += 1
    return counter


def base_blocks(seq, base):
    regexp = re.compile(base + "+")
    return regexp.findall(seq)


def detect_blocks_extremes(seq, base):
    regexp = re.compile("^(" + base + "+)*(.*?)(" + base + "+)*$")
    return regexp.findall(seq)


def trim_unknowns_extremes(seq):
    return seq.strip('N')


def write_seq_in_fasta_without_newlines(fastaHandle, seqRecord):
    fastaHandle.write('>' + seqRecord.id + "\n" + str(seqRecord.seq) + "\n")


def main():
    reads_filename = sys.argv[1]

    min_length = 20

    if len(sys.argv) == 3:
        min_length = int(sys.argv[2])

    trimmed_reads_filename = basename(reads_filename) + ".trimmed.fasta"

    trashed_reads_filename = basename(reads_filename) + ".trashed.fasta"

    reads_file = open(reads_filename, "r")

    trimmed_reads_file = open(trimmed_reads_filename, "w")

    trashed_reads_file = open(trashed_reads_filename, "w")

    i = 0
    reads_counter = 0

    trashed_reads = dict()

    for seqRecord in SeqIO.parse(reads_file, extension(reads_filename)):
        seq = str(seqRecord.seq)
        reads_counter += 1
        # print "-"
        # print seq
        # print len(seqRecord)
        # print countBases(seq)
        # n_blocks = base_blocks(seq,'N')
        # print "Qty N blocks: %d" % len (n_blocks)
        # print n_blocks
        trimmed = trim_unknowns_extremes(seq)
        # print "LEN TRIMMED: %d " % len(trimmed)

        if len(trimmed) >= min_length:
            seqRecord.letter_annotations = dict()
            seqRecord.seq = Seq(trimmed)
            write_seq_in_fasta_without_newlines(trimmed_reads_file, seqRecord)
        else:
            write_seq_in_fasta_without_newlines(trashed_reads_file, seqRecord)
            trashed_reads[seqRecord.id] = seqRecord
            # if i == 10: break
            # i += 1


    print "The trimmed reads were successfully written to " + trimmed_reads_filename

    print "There were trashed %d which did not possess the minimum length! " % (len(trashed_reads))
    print "The trashed reads were written to " + trashed_reads_filename

    commons.write_to_report([
        "Original reads file " + reads_filename + " has " + str(reads_counter) + " reads ",
        "There were trimmed " + str(len(trashed_reads))
    ])

if __name__ == '__main__':
    main()
