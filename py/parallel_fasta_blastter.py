import time

import get_config
from commons import *

__author__ = 'sam'


# query id, subject id, % identity, alignment length, mismatches, gap opens, q. start,
# q. end, s. start, s. end, evalue, bit score, sequence

def insert_blastresults_empty_file_headers(filename):
    writer = csv.writer(open(filename, "w"), delimiter='\t')
    headers = ["id", "sid", "p_id", "a_len", "mism", "g_op", "qst", "qend", "sst", "send", "eval", "bitsc"]
    writer.writerow(headers)




def main():
    fasta = sys.argv[1]
    seq_identity = sys.argv[2]
    print "Number of records in %s: %d " % (fasta, count_lines(fasta) / 2)
    before = time.time()

    blastresults_filename = basename(fasta) + ".blastresults"

    print "=> Preparing for BLAST, using FASTA file ", fasta
    print "=> The results will be saved to ", blastresults_filename

    executing("touch %s" % (blastresults_filename))
    insert_blastresults_empty_file_headers(blastresults_filename)

    blastdb = os.environ['BLASTDB']

    use_paralell = False

    use_megablast = False

    cpu_cores = int(get_config.use_how_many_cpus())

    default_blast_args = {
        'perc_identity': float(seq_identity),
        'db': blastdb,
        'num_threads': int(cpu_cores)
    }

    blast_args = default_blast_args

    if (os.environ.has_key('USE_PARALLEL') and os.environ['USE_PARALLEL'] == '1'):
        use_paralell = True

    if (os.environ.has_key('USE_MEGABLAST') and os.environ['USE_MEGABLAST'] == '1'):
        blast_args['task'] = 'megablast'


    if use_paralell:
        del (blast_args['num_threads'])
        run_blast_parallel(fasta, blastresults_filename, blast_args)
    else:
        run_blast(fasta, blastresults_filename, blast_args)

    print "Number of lines in %s: %s " % (blastresults_filename, (count_lines(blastresults_filename)))
    print "Filesize: ", os.path.getsize(blastresults_filename) / 1024 ** 2, " MB"

    after = time.time()
    print "Took %f seconds! " % (after - before)


if __name__ == '__main__':
    main()
