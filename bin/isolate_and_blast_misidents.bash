#!/usr/bin/env bash
DATASET_DIR=$1

python $HOME/work/pe-avaliator/py/directory_fasta_blastter_1by1.py \
--source_dir /home/sviana/work/pe-avaliator/data/Mock1_S1_L001/merged \
--seq_identity 70.0 \
--ref_dir /home/sviana/work/pe-avaliator/data/Mock1_S1_L001/fasta_organisms \
--ref_type BLASTDB \
--accession_organism_table /home/sviana/work/pe-avaliator/data/Mock1_S1_L001/analyzed/Mock1_S1_L001_R1_001.analyzed.organisms.tsv \
--blastresults_final_dir /home/sviana/work/pe-avaliator/data/Mock1_S1_L001/blastresults \
--temp_dir /home/sviana/work/pe-avaliator/data/Mock1_S1_L001/other
