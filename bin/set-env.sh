#!/bin/bash

echo " === Common global configuration === "

set -x
# specific host configuration
. conf/hosts/`hostname`.sh

set +x

# set the environment

. $VIRTUALENV/bin/activate
PATH=$PATH:$PE_AVALIATOR_PATH/bin:$PE_AVALIATOR_PATH/py
NUM_CPU_CORES=`cat /proc/cpuinfo  | grep processor | wc -l`


export PATH
export PE_AVALIATOR_PATH
export VIRTUALENV
export NUM_CPU_CORES
export USE_CORES
export BLASTDB
