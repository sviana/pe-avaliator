#!/usr/bin/env bash
EXEC=$1
LOGFILE=$EXEC".log"
echo "Logging to " $LOGFILE
exec 2>&1
exec &> >(tee -a $LOGFILE)

. $HOME/work/pe-avaliator/bin/set-env.sh
$PYTHON $EXEC
