#!/usr/bin/env bash

. bin/set-env-dataset.sh $1


loop_with_over_dir.bash "$PYTHON $SRC/fastquality_hister_using_counts.py" "$MERGED/*MERGED*.fastq"  $DATASET/loop_over_with-$DATASET_NAME.log

Rscript R/boxplots_from_score_counts.R $MERGED  data/scores_boxplot_$DATASET_NAME.png

