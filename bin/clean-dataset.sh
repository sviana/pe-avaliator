#!/usr/bin/env bash
DATASET=data/$1
BLASTRESULTS=$DATASET/blastresults
MERGED=$DATASET/merged
ANALYZED=$DATASET/analyzed
PICKLED=$DATASET/pickled
TSV=$DATASET/tsv
OTHER=$DATASET/other
ORGANISMS_DIR=$DATASET/fasta_organisms
FASTA_SINGLE_READS_DIR=$DATASET/fasta_single_reads

rm -v $ANALYZED/*
rm -v $BLASTRESULTS/*
#rm -v $MERGED/*
rm -v $OTHER/*
rm -v $TSV/*
rm -v $PICKLED/*
rm -v $DATASET/*.log
#rm -v $FASTA
rm -v $ORGANISMS_DIR/*
rm -v $FASTA_SINGLE_READS_DIR/*

#rm -v $ANALYZED/$FORWARD.trimmed.common.unique.fused.analyzed.blastresults
