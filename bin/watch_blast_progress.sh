#!/usr/bin/env bash
#$1 is the blastresults file
#$2 is the fasta file being blastted

watch -n2 "progress.sh 'python py/blast_progress.py $1 $2'"
