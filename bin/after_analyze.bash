#!/usr/bin/env bash

# some important variables
PROJ_DIR=$PE_AVALIATOR_PATH
DATASET_NAME=$1
DATASET=$PROJ_DIR/data/$DATASET_NAME
NUM_CPU_CORES=`cat /proc/cpuinfo  | grep processor | wc -l`
SRC=$PROJ_DIR/py
PYTHON=$VIRTUALENV/bin/python


# the forward and reverse filenames with reads

FORWARD="$DATASET_NAME"_R1_001
REVERSE="$DATASET_NAME"_R2_001

# if the forward and reverse filenames are different from the standard, provide them as parameters here
if [ $# == 3 ]; then
    FORWARD=$2
    REVERSE=$3
fi


export FORWARD
export REVERSE

# directories specific of the dataset
APRIORI=$DATASET/apriori
ORIGINAL=$DATASET/original
BLASTRESULTS=$DATASET/blastresults
ANALYZED=$DATASET/analyzed
PICKLED=$DATASET/pickled
TSV=$DATASET/tsv
OTHER=$DATASET/other
NEW_BLASTDB=$DATASET/blastdb

#some parameters
MINIMUM_LENGTH=20
USE_PARALLEL=0
USE_MEGABLAST=1
MIN_IDENTITY_PREPROCESS=90.0
MIN_IDENTITY_PPLINE=80.0

export USE_PARALLEL USE_MEGABLAST MIN_IDENTITY_PREPROCESS MIN_IDENTITY_PPLINE

#log to file and outputs normally
LOGFILE=$DATASET/ppline_"$DATASET_NAME".log
echo "Saving all output to $LOGFILE ..."
exec 2>&1
exec &> >(tee -a "$LOGFILE")


# run specific configuration of the dataset, possibly overriding some variable
set -x
. $DATASET/$DATASET_NAME.sh
set +x



#$PYTHON $SRC/fused_blastresults_negative-hunter.py $ANALYZED/$FORWARD.analyzed.blastresults \
#    $OTHER/$FORWARD.trimmed.common.unique.common.fused.fasta $DATASET-overlaps-stats.tsv
cp -v $ANALYZED/$FORWARD.analyzed.blastresults                    $APRIORI
mv -v $ANALYZED/$FORWARD.analyzed.overlaps.tsv                    $PICKLED/$FORWARD.positives-accessions.tsv
mv -v $ANALYZED/$FORWARD.analyzed.true_negatives_a_priori.tsv     $PICKLED/$FORWARD.negatives-accessions.tsv
mv -v $ANALYZED/$FORWARD.analyzed.missings.txt                    $PICKLED/$FORWARD.missing-accessions.txt

#
## now we are ready, having separated positives from negatives, to run the tool that analyzes the results of the mergers, and comparing the IDs present on the mergers results with the ones presents in the positives and negatives generated on the last step.
#
##but before that, and for performance reasons: we generate three 'pickled' files which are pure $PYTHON objects serialized in a file.
# Reading from these files is more quick than having to fetch the IDs from text files.
#
$PYTHON $SRC/pickle-all.py \
    $PICKLED/$FORWARD.positives-accessions.tsv \
    $PICKLED/$FORWARD.negatives-accessions.tsv  \
    $PICKLED/$FORWARD.missing-accessions.txt  \
    $ORIGINAL/$FORWARD.cleaned.fastq

mv -v $ORIGINAL/$FORWARD.cleaned.pickled $PICKLED

#creates a histogram of the overlaps, along with a tsv of that (needs R)

Rscript R/overlaps-histogr.R $PICKLED/$FORWARD.positives-accessions.tsv $DATASET/$FORWARD.overlaps-counts.tsv \
    $DATASET/$FORWARD.overlaps-hist.png




#generate the BLASTDB database, which ia a subset of the original one, containing only the sequences identified by the hits
# the blast pre-merge has done above

$PYTHON $SRC/subset_blastdb_using_organisms_found.py $ANALYZED/$FORWARD.analyzed.blastresults  $DATASET_NAME $NEW_BLASTDB
#mv -v $ANALYZED/*.blastdb.* $NEW_BLASTDB
