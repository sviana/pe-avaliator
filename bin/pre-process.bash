#!/bin/bash


. bin/set-env-dataset.sh $1


NEW_BLASTDB=$DATASET/blastdb

#log to file and outputs normally
LOGFILE=$DATASET/pre-process_"$DATASET_NAME".log
echo "Saving all output to $LOGFILE ..."
exec 2>&1
exec &> >(tee -a "$LOGFILE")



# turn the Fastq into Fasta

echo "Turning the original dataset FASTQ into FASTA..."

if [ !  -a $OTHER/$FORWARD.fasta ]; then
    echo -e "Start with $FORWARD.fastq"
    fastq_to_fasta -v -n -i  $ORIGINAL/$FORWARD.fastq -o $OTHER/$FORWARD.fasta
fi
if [ !  -a $OTHER/$REVERSE.fasta ]; then
    echo -e "... now with $REVERSE.fastq"
    fastq_to_fasta -v -n -i  $ORIGINAL/$REVERSE.fastq -o $OTHER/$REVERSE.fasta
fi

##creates two fasta files: one with trash sequeneces (*.trashed.fasta) and the other with the trimmed
## and untrimmed (that passed the trimming), specifying in the command line the minimum length of the
##resulting read



echo -e "Trimming the reads on fasta files:"
ls $OTHER/*.fasta
$PYTHON $SRC/reads_trimmer.py $OTHER/$FORWARD.fasta $MINIMUM_LENGTH
rm -v $OTHER/$FORWARD.fasta
$PYTHON $SRC/reads_trimmer.py $OTHER/$REVERSE.fasta $MINIMUM_LENGTH
rm -v $OTHER/$REVERSE.fasta

##we compare the two resulting files, and exclude the reads that have no pair in the other file
#
$PYTHON $SRC/fasta_intersect.py $OTHER/$FORWARD.trimmed.fasta $OTHER/$REVERSE.trimmed.fasta $OTHER/$FORWARD.trimmed.common.fasta
$PYTHON $SRC/fasta_intersect.py $OTHER/$REVERSE.trimmed.fasta $OTHER/$FORWARD.trimmed.common.fasta $OTHER/$REVERSE.trimmed.common.fasta
#rm -v $OTHER/*.trimmed.fasta
#
##detect the reads with identical sequences in both paired-end reads files, generating two FASTA files: one file with the repeated sequences, and other FASTA with the unique sequences
#
$PYTHON $SRC/uniqer.py $OTHER/$FORWARD.trimmed.common.fasta $OTHER/$REVERSE.trimmed.common.fasta
#rm -v $OTHER/*.trimmed.common.fasta

# let's do again the step of removing the reads that have no pair in the other file...

$PYTHON $SRC/fasta_intersect.py $OTHER/$FORWARD.trimmed.common.unique.fasta $OTHER/$REVERSE.trimmed.fasta $OTHER/$FORWARD.trimmed.common.unique.common.fasta
$PYTHON $SRC/fasta_intersect.py $OTHER/$REVERSE.trimmed.common.unique.fasta $OTHER/$FORWARD.trimmed.common.unique.common.fasta $OTHER/$REVERSE.trimmed.common.unique.common.fasta
#rm -v $OTHER/*.trimmed.common.unique.fasta

# we're going to generate new FASTQ files with only the IDs that are present on the last step

$PYTHON $SRC/subset_fastq_from_fasta.py $ORIGINAL/$FORWARD.fastq $OTHER/$FORWARD.trimmed.common.unique.common.fasta $ORIGINAL/$FORWARD.cleaned.fastq
$PYTHON $SRC/subset_fastq_from_fasta.py $ORIGINAL/$REVERSE.fastq $OTHER/$REVERSE.trimmed.common.unique.common.fasta $ORIGINAL/$REVERSE.cleaned.fastq

#cp -v $OTHER/$REVERSE.trimmed.common.unique.common.fasta $ORIGINAL


#"fuse" (ie join together) the paired-end reads FASTA files, in order to prepare them for BLAST, the reads MUST HAVE the same order as they have from the original, and the READS for the same ID must be grouped together ('forward' read and the 'reverse' one after another). A fasta file is generated with name $dataset.common.trimmed.unique.fused.fasta ($dataset is the common ID between  the paired-end reads file)
#
set -x
$PYTHON $SRC/fuse_paired_end_fastas.py \
    $OTHER/$FORWARD.trimmed.common.unique.common.fasta \
    $OTHER/$REVERSE.trimmed.common.unique.common.fasta \
    $OTHER/$DATASET_NAME.trimmed.common.unique.common.fused.fasta

#
#BLASTn over the 'fused' fasta, with the 80% minimal identity
#
#echo "Aborting..."
#exit
if [ ! -e $BLASTRESULTS/$DATASET_NAME.trimmed.common.unique.common.fused.blastresults ]; then
    $PYTHON $SRC/parallel_fasta_blastter.py $OTHER/$DATASET_NAME.trimmed.common.unique.common.fused.fasta $MIN_IDENTITY_PREPROCESS
    mv -v $OTHER/$DATASET_NAME.trimmed.common.unique.common.fused.blastresults $BLASTRESULTS
fi

#optional step: collect the reads which had no hits on BLAST, just for statistical purposes

$PYTHON $SRC/fused_blastresults_collect_missings.py $BLASTRESULTS/$DATASET_NAME.trimmed.common.unique.common.fused.blastresults \
    $OTHER/$DATASET_NAME.trimmed.common.unique.common.fused.fasta
mv -v $BLASTRESULTS/$DATASET_NAME.trimmed.common.unique.common.fused.blastresults.missings.txt $PICKLED/$DATASET_NAME.missings-accessions.txt


#
##analyze the BLASTn results, and then, for each read, get the organism that maximizes the identity between forward and reverse,
# the results are stored in one '.analyzed.blastresults' file which contains both reads  (forward and reverse) of the same pair
#

$PYTHON $SRC/fused_blastresults_analyzer.py  $BLASTRESULTS/$DATASET_NAME.trimmed.common.unique.common.fused.blastresults
mv -v $BLASTRESULTS/$DATASET_NAME.trimmed.common.unique.common.fused.analyzed.blastresults $ANALYZED/$DATASET_NAME.analyzed.blastresults
#
# then, we run the 'negative hunter' to catch the eventual negatives and only retain the positives (which possess a potential overlap,
# using the blastresults obtained above
# Five files are created: two TSVs with the positives and negatives with the important info from the blastresults
# a third file with just the IDs and overlaps
# a fourth with the IDs which did not had a pair (ie, doesn't had a BLAST hit for the paired read on the other direction!)
# and finally a file (which name is provided as a parameter) with the basic stats (average, sd) for the overlaps on the parent dir

$PYTHON $SRC/fused_blastresults_negative-hunter.py $ANALYZED/$DATASET_NAME.analyzed.blastresults \
    $OTHER/$DATASET_NAME.trimmed.common.unique.common.fused.fasta $DATASET-overlaps-stats.tsv
cp -v $ANALYZED/$DATASET_NAME.analyzed.blastresults                    $APRIORI
cp -v $ANALYZED/$DATASET_NAME.analyzed.positives_a_priori.tsv          $PICKLED/$DATASET_NAME.positives-accessions.tsv
cp -v $ANALYZED/$DATASET_NAME.analyzed.negatives_a_priori.tsv          $PICKLED/$DATASET_NAME.negatives-accessions.tsv
cp -v $ANALYZED/$DATASET_NAME.analyzed.unpaireds.txt                   $PICKLED/$DATASET_NAME.unpaireds-accessions.txt
cp -v $ANALYZED/$DATASET_NAME.analyzed.mispaireds.txt                  $PICKLED/$DATASET_NAME.mispaireds-accessions.txt

#mv -v $BLASTERSULTS/$DATASET_NAME.analyzed.missing-ids.txt             $PICKLED/$DATASET_NAME.missings-accessions.txt

#
## now we are ready, having separated positives from negatives, to run the tool that analyzes the results of the mergers, and comparing the IDs present on the mergers results with the ones presents in the positives and negatives generated on the last step.
#
##but before that, and for performance reasons: we generate three 'pickled' files which are pure $PYTHON objects serialized in a file. Reading from these files is more quick than having to fetch the IDs from text files.
#
$PYTHON $SRC/pickle-all.py \
    $PICKLED/$DATASET_NAME.positives-accessions.tsv \
    $PICKLED/$DATASET_NAME.negatives-accessions.tsv  \
    $PICKLED/$DATASET_NAME.unpaireds-accessions.txt  \
    $PICKLED/$DATASET_NAME.missings-accessions.txt  \
    $PICKLED/$DATASET_NAME.mispaireds-accessions.txt \
    $ORIGINAL/$FORWARD.cleaned.fastq

mv -v $ORIGINAL/$FORWARD.cleaned.pickled $PICKLED/$DATASET_NAME.cleaned.pickled

#creates a histogram of the overlaps, along with a tsv of that (needs R)

Rscript R/overlaps-histogr.R $PICKLED/$DATASET_NAME.positives-accessions.tsv $DATASET/$DATASET_NAME.overlaps-counts.tsv \
    $DATASET/$DATASET_NAME.overlaps-hist.png




#generate the BLASTDB database, which ia a subset of the original FASTA one, containing only the sequences identified by the hits
# the blast pre-merge has done above

$PYTHON $SRC/subset_blastdb_using_organisms_found.py $ANALYZED/$DATASET_NAME.analyzed.blastresults  $DATASET_NAME $NEW_BLASTDB


# split the FASTA generated in the last step into multiple FASTAS named for each organism found containing just one sequence
$PYTHON $SRC/divide_into_mono_seq_fastas.py $NEW_BLASTDB/`basename $BLASTDB`-$DATASET_NAME.fasta  $ORGANISMS_DIR

#
##finally, in a directory with the fastq files provenient of the merging and with the pickled files, we are ready to generate the statistics file!
#
#$PYTHON $SRC/mergers_statster.py fastq_location fasta_filenames_filter apriori_stats.tsv

#rm $OTHER/*

echo "Report file on " $REPORT_FILE
