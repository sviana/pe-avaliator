#!/usr/bin/env bash

#log to file and outputs normally
LOGFILE=$DATASET/$0-"$DATASET_NAME".log
echo "Saving all output to $LOGFILE ..."
exec 2>&1
exec &> >(tee -a "$LOGFILE")
