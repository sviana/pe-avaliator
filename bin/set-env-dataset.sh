#!/usr/bin/env bash
# some important variables
PROJ_DIR=$PE_AVALIATOR_PATH
DATASET_NAME=$1
DATASET=$PROJ_DIR/data/$DATASET_NAME
NUM_CPU_CORES=`cat /proc/cpuinfo  | grep processor | wc -l`
SRC=$PROJ_DIR/py
PYTHON=$VIRTUALENV/bin/python


# directories specific of the dataset
APRIORI=$DATASET/apriori
ORIGINAL=$DATASET/original
BLASTRESULTS=$DATASET/blastresults
MERGED=$DATASET/merged
ANALYZED=$DATASET/analyzed
PICKLED=$DATASET/pickled
TSV=$DATASET/tsv
OTHER=$DATASET/other
BLASTDB_DIR=$DATASET/blastdb

ORGANISMS_DIR=$DATASET/fasta_organisms
FASTA_SINGLE_READS_DIR=$HOME/tmp/fasta_single_reads

REPORT_FILE=$PROJ_DIR/data/$DATASET_NAME.report.txt

export BLASTDB
export PICKLED
export BLASTRESULTS
export MERGED
export ANALYZED
export TSV
export OTHER
export ORGANISMS_DIR
export FASTA_SINGLE_READS_DIR
export SRC
export PY
export REPORT_FILE

# expecting that the FORWARD and REVERSE env variables are already present in the environment... in case not,
# create them here...
FORWARD="$DATASET_NAME"_R1_001
REVERSE="$DATASET_NAME"_R2_001
#MERGED_NAME=`python -c "print '_'.join('$DATASET_NAME'.split('_')[:-2])"`
MERGED_NAME=$DATASET_NAME

# if the forward and reverse filenames are different from the standard, provide them as parameters here
if [ $# == 3 ]; then
    FORWARD=$2
    REVERSE=$3
fi


#some parameters
MINIMUM_LENGTH=20
USE_PARALLEL=0
USE_MEGABLAST=1
MIN_IDENTITY_PREPROCESS=90.0
MIN_IDENTITY_PPLINE=80.0
export USE_PARALLEL MIN_IDENTITY_PREPROCESS MIN_IDENTITY_PPLINE



# run specific configuration of the dataset, possibly overriding some variable
set -x
. $DATASET/$DATASET_NAME.sh
set +x

# Are we going to use parallel to blast ? outputs the env var value

echo  "USE PARALLEL? $USE_PARALLEL"

#ls -la $BLASTDB.*
echo "=> using BLASTDB: $BLASTDB"


export USE_PARALLEL USE_MEGABLAST MIN_IDENTITY_PREPROCESS MIN_IDENTITY_PPLINE PICKLED
