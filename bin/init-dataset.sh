#!/usr/bin/env bash

if [ -z  "$1" ]; then
    echo "No dataset name was provided!"
    exit 1
fi

set -v

NEW_DATASET_NAME=$1
NEW_DIR=data/$NEW_DATASET_NAME

mkdir -p $NEW_DIR
mkdir -p $NEW_DIR/original
mkdir -p $NEW_DIR/blastdb
mkdir -p $NEW_DIR/blastresults
mkdir -p $NEW_DIR/analyzed
mkdir -p $NEW_DIR/apriori
mkdir -p $NEW_DIR/merged
mkdir -p $NEW_DIR/other
mkdir -p $NEW_DIR/pickled
mkdir -p $NEW_DIR/tsv
mkdir -p $NEW_DIR/fasta_organisms
mkdir -p $NEW_DIR/fasta_merged
mkdir -p $NEW_DIR/fasta_single_reads

cp -v conf/lindy_hop.ini $NEW_DIR
cp -v conf/dataset_conf.sh $NEW_DIR/$NEW_DATASET_NAME.sh

set +v
