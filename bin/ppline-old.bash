#!/usr/bin/env bash

# some important variables
PROJ_DIR=$PE_AVALIATOR_PATH
DATASET_NAME=$1
DATASET=$PROJ_DIR/data/$DATASET_NAME
SRC=$PROJ_DIR/py
PYTHON=$VIRTUALENV/bin/python
BLASTDB=$HOME/work/SILVA/SILVA_119_SSURef_Nr99_tax_silva.DNA

# directories specific of the dataset
APRIORI=$DATASET/apriori
ORIGINAL=$DATASET/original
BLASTRESULTS=$DATASET/blastresults
MERGED=$DATASET/merged
ANALYZED=$DATASET/analyzed
PICKLED=$DATASET/pickled
TSV=$DATASET/tsv
OTHER=$DATASET/other
BLASTDB_DIR=$DATASET/blastdb
ORGANISMS_DIR=$DATASET/fasta_organisms
FASTA_SINGLE_READS_DIR=$HOME/tmp/fasta_single_reads

export BLASTDB
export PICKLED
export BLASTRESULTS
export MERGED
export ANALYZED
export TSV
export OTHER
export ORGANISMS_DIR
export FASTA_SINGLE_READS_DIR


# expecting that the FORWARD and REVERSE env variables are already present in the environment... in case not,
# create them here...
FORWARD="$DATASET_NAME"_R1_001
REVERSE="$DATASET_NAME"_R2_001
#MERGED_NAME=`python -c "print '_'.join('$DATASET_NAME'.split('_')[:-2])"`
MERGED_NAME=$DATASET_NAME
# some parameters...

# if the forward and reverse filenames are different from the standard, provide them as parameters here
if [ $# == 3 ]; then
    FORWARD=$2
    REVERSE=$3
fi

USE_PARALLEL=0
MIN_IDENTITY_PREPROCESS=90.0
MIN_IDENTITY_PPLINE=80.0
export USE_PARALLEL MIN_IDENTITY_PREPROCESS MIN_IDENTITY_PPLINE

#log to file and outputs normally
LOGFILE=$DATASET/ppline_"$DATASET_NAME".log
echo "Saving all output to $LOGFILE ..."
exec 2>&1
exec &> >(tee -a "$LOGFILE")


# run specific configuration of the dataset, possibly overriding some variable
set -x
. $DATASET/$DATASET_NAME.sh
set +x

# Are we going to use parallel to blast ? outputs the env var value

echo  "USE PARALLEL? $USE_PARALLEL"

#set new blastdb to the specific made as a subset with only the hits found
if [ -v USE_BLASTDB_HITS ] &&  [ $USE_BLASTDB_HITS -eq 1 ]
    then
    BLASTDB=$BLASTDB_DIR/`basename $BLASTDB`-`basename $DATASET.blastdb`
fi
#ls -la $BLASTDB.*
echo "=> using BLASTDB: $BLASTDB"


# executes the mergers in batch
$PYTHON $SRC/lindy_hop.py --conf $DATASET/lindy_hop.ini \
 --overlap_stats_file $PROJ_DIR/data/$DATASET_NAME-overlaps-stats.tsv \
 $ORIGINAL/$FORWARD.cleaned.fastq \
 $ORIGINAL/$REVERSE.cleaned.fastq \
 $MERGED/"$MERGED_NAME"_MERGED

#sexit -1

#take the resulting FASTQ resulting from the merge that are placed in a directory
# and feeds them to a blastn with some parameters like the percent identity and
# max target seqs

#$PYTHON $SRC/directory_parallel_fasta_blastter.py $MERGED $MIN_IDENTITY_PPLINE 500 $BLASTRESULTS
$PYTHON $SRC/directory_fasta_blastter_1by1.py \
    --source_dir $MERGED \
    --seq_identity $MIN_IDENTITY_PPLINE \
    --ref_dir $ORGANISMS_DIR \
    --ref_type blastdb \
    --accession_organism_table  $ANALYZED/$DATASET_NAME.analyzed.organisms.tsv \
    --blastresults_final_dir $BLASTRESULTS \
    --temp_dir $OTHER

mv -v $MERGED/*.blastresults $BLASTRESULTS

#compare the blastresults file from each MERGED fastq with the blastresults before merge (with the intention
# of finding which after-merge results maps with the species that maximizes sequence identity
# this command must be executed for each blastresults file

for blastresults_file in $BLASTRESULTS/*.blastresults; do
    $PYTHON $SRC/pre_and_pos_merge_blastresults_comparator.py $blastresults_file $APRIORI/$DATASET_NAME.analyzed.blastresults
    mv -v $BLASTRESULTS/*.analyzed* $ANALYZED
done

#then, compare the blast results before and after BLAST on the mismatches parameter

$PYTHON $SRC/collect-mismoverlaps-mergers-blastresults-after-merge.py $ORIGINAL/$FORWARD.cleaned.fastq $ANALYZED $TSV/$DATASET_NAME-mismatches.tsv

# after this, using one file with the mismatches values that come from the original read files before and after merge,
# we can obtain the mismatch differences before the merging, this script generates a tabular format file (tsv) with one
# merger per column, columns mismatcheswith "-" means that the read were not merged for that particular merger, negative alues
# are true positives in which the mismatches value where reduced comparing withe the after-merge.

$PYTHON $SRC/before_and_after_mismatches_differ.py  $APRIORI/$DATASET_NAME.analyzed.blastresults $TSV/$DATASET_NAME-mismatches.tsv


# the output file (extension .diff.tsv) is analyzed search for those reads that had less mismatches after merge (those in which
# the values are less than 0). Its Id's are stored in a text file, one for each merger in each column,

$PYTHON $SRC/mismdiffscatcher.py  $TSV/$DATASET_NAME-mismatches.diff.tsv

# generate text files with the read id's which were merged (one per line)

for fasta in $MERGED/*.fasta; do
    $PYTHON $SRC/colllect_ids_from_fastx.py $fasta
    mv -v $MERGED/*.ids.txt $OTHER
done

# generate text files with IDs

$PYTHON $SRC/mergers_statster_with_ids.py \
    --merged_accessions_dir $OTHER \
    --merged_accessions_filenames_filter *.fasta.ids.txt \
    --dataset_name  $DATASET_NAME \
    --stats_filename $DATASET-final_all.tsv \
    --diff_accessions_dir $TSV \
    --diff_accessions_filenames_filter *.tsv

#count the quality scores of the mergers and generate a boxplot with all of them

#. bin/scores_stats.bash $DATASET

#less $DATASET-final_all.tsv

#xdg-open $DATASET-final_all.tsv

# notify by email the end of execution

#. ./notify-end.bash
