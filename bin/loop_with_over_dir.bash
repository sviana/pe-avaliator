#!/usr/bin/env bash
EXEC=$1
DIR=$2
outfile=$3
extra_args=$4

echo -e "Looping with '$EXEC' over '$DIR' , and logging it to $outfile"


for file in $DIR ; do
	echo $file
    $EXEC $file $queue_size >> $outfile
done
